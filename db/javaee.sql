/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50712
Source Host           : localhost:3306
Source Database       : javaee

Target Server Type    : MYSQL
Target Server Version : 50712
File Encoding         : 65001

Date: 2017-12-28 17:38:24
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `soft_classroom`
-- ----------------------------
DROP TABLE IF EXISTS `soft_classroom`;
CREATE TABLE `soft_classroom` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `classroom_name` varchar(255) DEFAULT NULL,
  `classroom_create_date` varchar(255) DEFAULT NULL,
  `classroom_create_user_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of soft_classroom
-- ----------------------------
INSERT INTO `soft_classroom` VALUES ('1', '高三（12）班', '2017-12-21 22:18:33', 'teacher02');
INSERT INTO `soft_classroom` VALUES ('2', '高三（13）班', '2017-12-07 01:18:22', 'admin');
INSERT INTO `soft_classroom` VALUES ('3', '高二（5）班', '2017-12-07 01:18:22', 'admin');
INSERT INTO `soft_classroom` VALUES ('4', '三年级（2）班', '2017-12-07 01:18:22', 'admin');

-- ----------------------------
-- Table structure for `soft_file`
-- ----------------------------
DROP TABLE IF EXISTS `soft_file`;
CREATE TABLE `soft_file` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of soft_file
-- ----------------------------

-- ----------------------------
-- Table structure for `soft_log`
-- ----------------------------
DROP TABLE IF EXISTS `soft_log`;
CREATE TABLE `soft_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `log_clazz` varchar(255) DEFAULT NULL,
  `log_create_date` varchar(255) DEFAULT NULL,
  `log_ip_add` varchar(255) DEFAULT NULL,
  `log_method` varchar(255) DEFAULT NULL,
  `log_msg` varchar(255) DEFAULT NULL,
  `log_username` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=583 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of soft_log
-- ----------------------------
INSERT INTO `soft_log` VALUES ('1', 'org.ssh2.web.controller.UserController', '2017-12-18 22:51:17', '0:0:0:0:0:0:0:1', 'doLogin', '用户登陆', 'teacher02');
INSERT INTO `soft_log` VALUES ('2', 'org.ssh2.web.controller.UserController', '2017-12-18 22:52:41', '0:0:0:0:0:0:0:1', 'doLogin', '用户登陆', 'teacher02');
INSERT INTO `soft_log` VALUES ('3', 'org.ssh2.web.controller.SelectCourseController', '2017-12-18 22:52:47', '0:0:0:0:0:0:0:1', 'approvallistPrompt', '选课审核页面跳转', 'teacher02');
INSERT INTO `soft_log` VALUES ('4', 'org.ssh2.web.controller.UserController', '2017-12-18 22:52:56', '0:0:0:0:0:0:0:1', 'toAdd', '用户toAdd', 'teacher02');
INSERT INTO `soft_log` VALUES ('5', 'org.ssh2.web.controller.ProjectController', '2017-12-18 22:53:21', '0:0:0:0:0:0:0:1', 'addPrompt', '发布课程设计addPrompt', 'teacher02');
INSERT INTO `soft_log` VALUES ('6', 'org.ssh2.web.controller.TaskController', '2017-12-18 22:53:23', '0:0:0:0:0:0:0:1', 'addPrompt', '发布任务addPrompt', 'teacher02');
INSERT INTO `soft_log` VALUES ('7', 'org.ssh2.web.controller.UserController', '2017-12-18 22:57:47', '0:0:0:0:0:0:0:1', 'doLogin', '用户登陆', 'teacher02');
INSERT INTO `soft_log` VALUES ('8', 'org.ssh2.web.controller.SelectCourseController', '2017-12-18 22:57:51', '0:0:0:0:0:0:0:1', 'approvallistPrompt', '选课审核页面跳转', 'teacher02');
INSERT INTO `soft_log` VALUES ('9', 'org.ssh2.web.controller.UserController', '2017-12-18 22:58:43', '0:0:0:0:0:0:0:1', 'doLogin', '用户登陆', 'teacher02');
INSERT INTO `soft_log` VALUES ('10', 'org.ssh2.web.controller.SelectCourseController', '2017-12-18 22:58:45', '0:0:0:0:0:0:0:1', 'approvallistPrompt', '选课审核页面跳转', 'teacher02');
INSERT INTO `soft_log` VALUES ('11', 'org.ssh2.web.controller.SelectCourseController', '2017-12-18 22:58:54', '0:0:0:0:0:0:0:1', 'approvallistPrompt', '选课审核页面跳转', 'teacher02');
INSERT INTO `soft_log` VALUES ('12', 'org.ssh2.web.controller.SelectCourseController', '2017-12-18 22:59:01', '0:0:0:0:0:0:0:1', 'approvallistPrompt', '选课审核页面跳转', 'teacher02');
INSERT INTO `soft_log` VALUES ('13', 'org.ssh2.web.controller.TaskController', '2017-12-18 22:59:25', '0:0:0:0:0:0:0:1', 'addPrompt', '发布任务addPrompt', 'teacher02');
INSERT INTO `soft_log` VALUES ('14', 'org.ssh2.web.controller.TaskController', '2017-12-18 22:59:27', '0:0:0:0:0:0:0:1', 'addPrompt', '发布任务addPrompt', 'teacher02');
INSERT INTO `soft_log` VALUES ('15', 'org.ssh2.web.controller.TaskController', '2017-12-18 22:59:32', '0:0:0:0:0:0:0:1', 'addPrompt', '发布任务addPrompt', 'teacher02');
INSERT INTO `soft_log` VALUES ('16', 'org.ssh2.web.controller.TaskController', '2017-12-18 22:59:35', '0:0:0:0:0:0:0:1', 'addPrompt', '发布任务addPrompt', 'teacher02');
INSERT INTO `soft_log` VALUES ('17', 'org.ssh2.web.controller.ProjectController', '2017-12-18 22:59:54', '0:0:0:0:0:0:0:1', 'addPrompt', '发布课程设计addPrompt', 'teacher02');
INSERT INTO `soft_log` VALUES ('18', 'org.ssh2.web.controller.UserController', '2017-12-18 23:04:55', '0:0:0:0:0:0:0:1', 'doLogin', '用户登陆', 'teacher02');
INSERT INTO `soft_log` VALUES ('568', 'org.ssh2.web.controller.UserController', '2017-12-28 17:37:05', '0:0:0:0:0:0:0:1', 'doLogin', '用户登陆', 'teacher02');
INSERT INTO `soft_log` VALUES ('569', 'org.ssh2.web.controller.SelectCourseController', '2017-12-28 17:37:26', '0:0:0:0:0:0:0:1', 'listPrompt', '选课listPrompt', 'teacher02');
INSERT INTO `soft_log` VALUES ('570', 'org.ssh2.web.controller.SelectCourseController', '2017-12-28 17:37:28', '0:0:0:0:0:0:0:1', 'listPrompt', '选课listPrompt', 'teacher02');
INSERT INTO `soft_log` VALUES ('571', 'org.ssh2.web.controller.SelectCourseController', '2017-12-28 17:37:33', '0:0:0:0:0:0:0:1', 'approvallistPrompt', '选课审核页面跳转', 'teacher02');
INSERT INTO `soft_log` VALUES ('572', 'org.ssh2.web.controller.ProjectController', '2017-12-28 17:37:42', '0:0:0:0:0:0:0:1', 'listProject', '查询课程设计全部', 'teacher02');
INSERT INTO `soft_log` VALUES ('573', 'org.ssh2.web.controller.SelectCourseController', '2017-12-28 17:37:42', '0:0:0:0:0:0:0:1', 'approvallistPrompt', '选课审核页面跳转', 'teacher02');
INSERT INTO `soft_log` VALUES ('574', 'org.ssh2.web.controller.SelectCourseController', '2017-12-28 17:37:43', '0:0:0:0:0:0:0:1', 'approvallistPrompt', '选课审核页面跳转', 'teacher02');
INSERT INTO `soft_log` VALUES ('575', 'org.ssh2.web.controller.ProjectController', '2017-12-28 17:37:44', '0:0:0:0:0:0:0:1', 'addPrompt', '发布课程设计addPrompt', 'teacher02');
INSERT INTO `soft_log` VALUES ('576', 'org.ssh2.web.controller.TaskController', '2017-12-28 17:37:46', '0:0:0:0:0:0:0:1', 'addPrompt', '发布任务addPrompt', 'teacher02');
INSERT INTO `soft_log` VALUES ('577', 'org.ssh2.web.controller.SelectCourseController', '2017-12-28 17:37:49', '0:0:0:0:0:0:0:1', 'approvallistPrompt', '选课审核页面跳转', 'teacher02');
INSERT INTO `soft_log` VALUES ('578', 'org.ssh2.web.controller.SelectCourseController', '2017-12-28 17:37:49', '0:0:0:0:0:0:0:1', 'listPrompt', '选课listPrompt', 'teacher02');
INSERT INTO `soft_log` VALUES ('579', 'org.ssh2.web.controller.SelectCourseController', '2017-12-28 17:37:53', '0:0:0:0:0:0:0:1', 'approvallistPrompt', '选课审核页面跳转', 'teacher02');
INSERT INTO `soft_log` VALUES ('580', 'org.ssh2.web.controller.SelectCourseController', '2017-12-28 17:37:55', '0:0:0:0:0:0:0:1', 'updatePrompt', '跳转更新页面给分数', 'teacher02');
INSERT INTO `soft_log` VALUES ('581', 'org.ssh2.web.controller.SelectCourseController', '2017-12-28 17:38:03', '0:0:0:0:0:0:0:1', 'approvallistPrompt', '选课审核页面跳转', 'teacher02');
INSERT INTO `soft_log` VALUES ('582', 'org.ssh2.web.controller.SelectCourseController', '2017-12-28 17:38:03', '0:0:0:0:0:0:0:1', 'listPrompt', '选课listPrompt', 'teacher02');

-- ----------------------------
-- Table structure for `soft_project`
-- ----------------------------
DROP TABLE IF EXISTS `soft_project`;
CREATE TABLE `soft_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_create_date` varchar(255) DEFAULT NULL,
  `project_end_date` varchar(255) DEFAULT NULL,
  `project_byteacher` varchar(255) DEFAULT NULL,
  `project_desc` varchar(255) DEFAULT NULL,
  `project_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `project_name` (`project_name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of soft_project
-- ----------------------------
INSERT INTO `soft_project` VALUES ('1', '2017-12-07 01:18:22', null, 'teacher02', '课程设计', '课程设计');
INSERT INTO `soft_project` VALUES ('2', '2017-12-07 01:18:22', null, 'teacher01', '数据库课程设计', '数据库课程设计');

-- ----------------------------
-- Table structure for `soft_role`
-- ----------------------------
DROP TABLE IF EXISTS `soft_role`;
CREATE TABLE `soft_role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_create_date` varchar(255) DEFAULT NULL,
  `role_create_user_name` varchar(255) DEFAULT NULL,
  `role_desc` varchar(255) DEFAULT NULL,
  `role_name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of soft_role
-- ----------------------------
INSERT INTO `soft_role` VALUES ('1', '2017-12-21 22:12:25', 'teacher02', '管理员', 'ROLE_ADMIN');
INSERT INTO `soft_role` VALUES ('2', '2017-12-21 22:04:04', 'teacher02', '老师', 'ROLE_TEACHER');
INSERT INTO `soft_role` VALUES ('3', '2017-12-21 21:51:03', 'teacher02', '学生', 'ROLE_STUDENT');

-- ----------------------------
-- Table structure for `soft_selectcourse`
-- ----------------------------
DROP TABLE IF EXISTS `soft_selectcourse`;
CREATE TABLE `soft_selectcourse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_approver` varchar(255) DEFAULT NULL,
  `app_create_date` varchar(255) DEFAULT NULL,
  `app_deal_date` varchar(255) DEFAULT NULL,
  `app_desc` varchar(255) DEFAULT NULL,
  `project_name` varchar(255) DEFAULT NULL,
  `select_name` varchar(255) DEFAULT NULL,
  `app_status` varchar(255) DEFAULT NULL,
  `task_name` varchar(255) DEFAULT NULL,
  `score` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of soft_selectcourse
-- ----------------------------
INSERT INTO `soft_selectcourse` VALUES ('1', '', '2017-12-18 23:05:14', null, '申请', '课程设计', '01', '未审批', '构建springboot', null);
INSERT INTO `soft_selectcourse` VALUES ('2', 'teacher02', '2017-12-18 23:05:14', '2017-12-18 23:05:14', '申请', '课程设计', '02', '已审批', '集成struts', '100');
INSERT INTO `soft_selectcourse` VALUES ('3', 'teacher01', '2017-12-18 23:05:14', '2017-12-19 21:59:10', '申请', '课程设计', '06', '已审批', '集成struts', null);
INSERT INTO `soft_selectcourse` VALUES ('15', 'teacher02', '2017-12-18 23:05:14', '2017-12-18 23:05:14', '申请', '数据库课程设计', 'teacher02', '已审批', '设计数据表', null);
INSERT INTO `soft_selectcourse` VALUES ('16', null, '2017-12-18 23:05:14', null, '申请', '数据库课程设计', 'teacher02', '未审批', '设计数据表', null);
INSERT INTO `soft_selectcourse` VALUES ('17', null, '2017-12-18 23:05:14', null, '申请', '数据库课程设计', '03', '未审批', '设计数据表', null);
INSERT INTO `soft_selectcourse` VALUES ('24', null, '2017-12-18 23:05:14', null, '申请', '数据库课程设计', '05', '未审批', '设计数据表', null);
INSERT INTO `soft_selectcourse` VALUES ('25', null, '2017-12-18 23:05:14', null, '申请', '数据库课程设计', '04', '未审批', '设计数据表', null);

-- ----------------------------
-- Table structure for `soft_task`
-- ----------------------------
DROP TABLE IF EXISTS `soft_task`;
CREATE TABLE `soft_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_create_date` varchar(255) DEFAULT NULL,
  `task_end_date` varchar(255) DEFAULT NULL,
  `task_byteacher` varchar(255) DEFAULT NULL,
  `task_desc` varchar(255) DEFAULT NULL,
  `task_name` varchar(255) NOT NULL,
  `project_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `task_name` (`task_name`),
  KEY `FK847F047ABF1CE89A` (`project_id`),
  CONSTRAINT `FK847F047ABF1CE89A` FOREIGN KEY (`project_id`) REFERENCES `soft_project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of soft_task
-- ----------------------------
INSERT INTO `soft_task` VALUES ('1', '2017-12-07 01:18:22', null, 'teacher02', '集成struts', '集成struts', '1');
INSERT INTO `soft_task` VALUES ('2', null, null, 'teacher02', '构建springboot', '构建springboot', '1');
INSERT INTO `soft_task` VALUES ('3', null, null, 'teacher01', '画e-r图', '画e-r图', '2');
INSERT INTO `soft_task` VALUES ('4', null, null, 'teacher01', '设计数据表', '设计数据表', '2');

-- ----------------------------
-- Table structure for `soft_user`
-- ----------------------------
DROP TABLE IF EXISTS `soft_user`;
CREATE TABLE `soft_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_create_date` varchar(255) DEFAULT NULL,
  `user_address` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_password` varchar(255) DEFAULT NULL,
  `user_realname` varchar(255) DEFAULT NULL,
  `user_sex` varchar(255) DEFAULT NULL,
  `user_telephone` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of soft_user
-- ----------------------------
INSERT INTO `soft_user` VALUES ('1', '2017-12-08 16:59:10', '北京', '123@qq.com', 'admin', 'admin', '管理员', '男', '123456789');
INSERT INTO `soft_user` VALUES ('2', '2017-12-08 16:59:10', null, '123@qq.com', 'teacher01', 'teacher', '王老师', '男', null);
INSERT INTO `soft_user` VALUES ('3', '2017-12-08 16:59:10', null, '123@qq.com', 'teacher02', 'teacher', '李老师', '女', null);
INSERT INTO `soft_user` VALUES ('4', '2017-12-08 16:59:10', '南宁', '123@qq.com', '01', '01', '李子明', '男', '123456789');
INSERT INTO `soft_user` VALUES ('5', '2017-12-08 16:59:10', '南宁', '123@qq.com', '02', '02', '王小明', '男', '123456789');
INSERT INTO `soft_user` VALUES ('6', '2017-12-08 16:59:10', '南宁', '123@qq.com', '03', '03', '李敏君', '女', '123456789');
INSERT INTO `soft_user` VALUES ('7', '2017-12-08 16:59:10', '南宁', '123@qq.com', '04', '04', '林景锋', '男', '123456789');
INSERT INTO `soft_user` VALUES ('8', '2017-12-08 16:59:10', null, '123@qq.com', '05', '05', '孙亮', '男', null);

-- ----------------------------
-- Table structure for `soft_user_classroom`
-- ----------------------------
DROP TABLE IF EXISTS `soft_user_classroom`;
CREATE TABLE `soft_user_classroom` (
  `user_id` int(11) NOT NULL,
  `classroom_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`classroom_id`),
  KEY `FKADDFF3945980065A` (`classroom_id`),
  KEY `FKADDFF394C98505A` (`user_id`),
  CONSTRAINT `FKADDFF3945980065A` FOREIGN KEY (`classroom_id`) REFERENCES `soft_classroom` (`id`),
  CONSTRAINT `FKADDFF394C98505A` FOREIGN KEY (`user_id`) REFERENCES `soft_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of soft_user_classroom
-- ----------------------------
INSERT INTO `soft_user_classroom` VALUES ('2', '1');
INSERT INTO `soft_user_classroom` VALUES ('4', '1');
INSERT INTO `soft_user_classroom` VALUES ('2', '2');
INSERT INTO `soft_user_classroom` VALUES ('5', '3');
INSERT INTO `soft_user_classroom` VALUES ('3', '4');
INSERT INTO `soft_user_classroom` VALUES ('6', '4');
INSERT INTO `soft_user_classroom` VALUES ('7', '4');
INSERT INTO `soft_user_classroom` VALUES ('8', '4');

-- ----------------------------
-- Table structure for `soft_user_project`
-- ----------------------------
DROP TABLE IF EXISTS `soft_user_project`;
CREATE TABLE `soft_user_project` (
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`project_id`),
  KEY `FKFB98EC9ABF1CE89A` (`project_id`),
  KEY `FKFB98EC9AC98505A` (`user_id`),
  CONSTRAINT `FKFB98EC9ABF1CE89A` FOREIGN KEY (`project_id`) REFERENCES `soft_project` (`id`),
  CONSTRAINT `FKFB98EC9AC98505A` FOREIGN KEY (`user_id`) REFERENCES `soft_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of soft_user_project
-- ----------------------------

-- ----------------------------
-- Table structure for `soft_user_role`
-- ----------------------------
DROP TABLE IF EXISTS `soft_user_role`;
CREATE TABLE `soft_user_role` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FK25573695676D8C7A` (`role_id`),
  KEY `FK25573695C98505A` (`user_id`),
  CONSTRAINT `FK25573695676D8C7A` FOREIGN KEY (`role_id`) REFERENCES `soft_role` (`id`),
  CONSTRAINT `FK25573695C98505A` FOREIGN KEY (`user_id`) REFERENCES `soft_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of soft_user_role
-- ----------------------------
INSERT INTO `soft_user_role` VALUES ('1', '1');
INSERT INTO `soft_user_role` VALUES ('2', '2');
INSERT INTO `soft_user_role` VALUES ('3', '2');
INSERT INTO `soft_user_role` VALUES ('4', '3');
INSERT INTO `soft_user_role` VALUES ('5', '3');
INSERT INTO `soft_user_role` VALUES ('6', '3');
INSERT INTO `soft_user_role` VALUES ('7', '3');
INSERT INTO `soft_user_role` VALUES ('8', '3');
