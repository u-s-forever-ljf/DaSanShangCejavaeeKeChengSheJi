<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>欢迎登录后台管理系统</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="" />
<meta name="author" content="" />

<!-- CSS -->
<link rel='stylesheet'
	href='http://fonts.googleapis.com/css?family=PT+Sans:400,700'>
<link rel="stylesheet" href="assets/css/reset.css">
<link rel="stylesheet" href="assets/css/supersized.css">
<link rel="stylesheet" href="assets/css/style.css">
<script type="text/javascript" src="js/jquery.js"></script>

<script type="text/javascript" src="resources/lib/jquery-1.7.1.min.js" />

<script type="text/javascript"
	src="resources/lib/jquery.json-2.3.min.js">
</script>
<script type="text/javascript"
	src="resources/lib/jquery.i18n.properties-1.0.9.js">
</script>
<script type="text/javascript" src="resources/js/main.js"></script>

</head>

<body>

	<div class="page-container">
		<h1>Login</h1>
		<form name="loginForm" action="<%=basePath%>j_spring_security_check"
			method="post">
			<table width="350px">
			<tr style="width: 350px"><td id="label_username" style="width: 100px"></td>
			<td><input  type="text" name="user.userName" class="username" placeholder="Username" value="teacher02"/></td></tr>
			<tr style="width: 350px"><td id="label_password" style="width: 100px"></td>
			<td><input  type="password" name="user.userPassword" class="password" placeholder="Password" value="teacher"/></td></tr>
			<tr style="width: 350px"><td style="width: 100px"></td><td><button id="button_login" type="submit"></button></td></tr>
			</table>
		</form>

		<br /> <br />
	</div>
	<div align="center">
		欢迎fork我的代码 <a
			href="https://gitee.com/u-s-forever-ljf/DaSanShangCejavaeeKeChengSheJi"
			target="_blank" title="项目管理">大三上册课程设计</a>
	</div>

	<!-- Javascript -->

	<script src="assets/js/supersized.3.2.7.min.js"></script>
	<script src="assets/js/supersized-init.js"></script>
	<script src="assets/js/scripts.js"></script>

</body>

</html>
