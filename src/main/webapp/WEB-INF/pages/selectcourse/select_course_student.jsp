<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>选课申请</title>
<link href="css/css/style.css" rel="stylesheet" type="text/css" />
<link href="css/css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="css/js/jquery.js"></script>
<script type="text/javascript" src="css/js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="css/js/select-ui.min.js"></script>
<script type="text/javascript" src="css/editor/kindeditor.js"></script>
<script type="text/javascript">
    KE.show({
        id : 'content7',
        cssPath : './index.css'
    });
</script> 
<script type="text/javascript">
	$(document).ready(function(e) {
		$(".select1").uedSelect({
			width : 345
		});
		$(".select2").uedSelect({
			width : 180
		});
		$(".select3").uedSelect({
			width : 200
		});
	});
</script>
<script type="text/javascript">
	//页面加载时首先获得课程设计
	$(function() {
		$.ajax({
			type : "post",
			url : "project",
			dataType : "json",
			async : false,
			success : function(data) {
				//alert(data);
				var pro = eval("(" + data + ")");
				for (var i = 0; i < pro.length; i++) {
					$("#project").append("<option value=" + pro[i].id + ">" + pro[i].projectName + "</option>");
				}
			}
		});
	});
</script>
</head>
<body>
	<div class="place">
		<span>位置：</span>
		<ul class="placeul">
			<li><a href="PageFrame_index.action">首页</a></li>
		</ul>
	</div>
	<div class="formbody">
		<div id="usual1" class="usual">
			<div class="itab">
				<ul>
					<li><a href="#tab1" class="selected">申请</a></li>
				</ul>
			</div>

			<form name="addApply" action="SelectCourse_doSave" method="post">
				<div id="tab1">

					<div class="formtext">
						Hi，<b>${user.userName}</b>，欢迎您试用选课功能！
					</div>
					<!-- 三级联动参考   http://blog.csdn.net/luckey_zh/article/details/22995389 -->
					<ul class="forminfo">
						<li><label>课程设计<b>*</b></label><img src="css/images/d02.png" />
							<div class="vocation">
								<!-- 如果使用 select1 可能导致取不到值-->
								<select id="project" name="project" class="select3">
									<option value="0">--请选择--</option>
								</select>
							</div></li>
						<li><input type="hidden" id="projectId" name="projectId"
							value="" /></li>
						<li id=""><label>任务</label><img src="css/images/d02.png" /> 
						<select name="task" id="task"
							class="select3">
								<option selected="selected" value="">任务：</option>
								<s:iterator value="taskId">
									<option value="${taskId}">${taskName}</option>
								</s:iterator>
						</select></li>
						<li><input type="hidden" id="taskprojectId" name="taskprojectId" value="" />
							<li><input type="hidden" id="" name=selectCourse.status
								" type="text" class="dfinput" value="未审批" /></li>
							<li><label>&nbsp;</label><input id="createApply" name=""
								type="submit" class="btn" value="马上申请" /></li>
					</ul>
				</div>
			</form>
		</div>
		<script type="text/javascript">
			$(function() {
				$('#createApply').click(function() {
					var project = $('#project').val();
					var task = $('#taskId').val();
					if (project == '') {
						alert('请选择课程设计！');
						return false;
					}
					if (task == '') {
						alert('请选择任务！');
						return false;
					}
					//	$("#projectId").val=$("#project").val();
					//	$("#taskIdss").val=$("#taskId").val();
					$('#addApply').submit();
				});
			});
		</script>
		<script type="text/javascript">
			$(function() {
				$("#project").change(function() {
					var url = "SelectCourse_findTask.action";
					var projectId = $("#project").val();
					$("#taskId option:not(:first)").remove();
					$.post(url, {
						"projectId" : projectId
					}, function(data) {
						/* alert("运行到2");
						alert(data); */
						var result = data.result;
						for (var i = 0; i < result.length; i++) {
							$("#task").append("<option  value='" + result[i].taskId + "'>" + result[i].taskName + "</option>");
						}
					}, "json");
		
				});
			});
		</script>
		
		
		<script type="text/javascript">
			$("#task").change(function() {
				$("#taskprojectId").attr("value", $("#task").val());
			});
			$("#project").change(function() {
				$("#projectId").attr("value", $("#project").val());
			});
		</script>


		<script type="text/javascript">
			$("#usual1 ul").idTabs();
		</script>

		<script type="text/javascript">
			$('.tablelist tbody tr:odd').addClass('odd');
		</script>




	</div>


</body>

</html>
