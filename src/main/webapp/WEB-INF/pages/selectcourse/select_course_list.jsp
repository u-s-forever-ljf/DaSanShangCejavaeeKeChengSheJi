<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>选</title>
<link href="css/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<!-- 全选/全不选 -->

<script type="text/javascript">

	$(function() {
		$("#checkAll").click(function() {

			$('input[name="ids"]').attr("checked", this.checked);
		});
		var $ids = $("input[name='ids']");
		$ids.click(function() {
			$("checkAll").attr("checked", $ids.length == $("input[name='ids']:checked").length ? true : false);
		});
	});
</script>
<script type="text/javascript">
$(document).ready(function(){
  $(".click").click(function(){
  $(".tip").fadeIn(200);
  });
  
  $(".tiptop a").click(function(){
  $(".tip").fadeOut(200);
});

  $(".sure").click(function(){
  $(".tip").fadeOut(100);
});

  $(".cancel").click(function(){首页
  
  $(".tip").fadeOut(100);
});
  
  //页面加载完成后排序
  reSort();
});

//默认优先显示未检测
var doFirst = false;

function reSort(){
	if(doFirst){
		//优先显示已审批
		$("#doNot_first").hide();
		$("#do_first").show();
		doFirst = false;
	}else{
		//优先显示未审批
		$("#doNot_first").show();
		$("#do_first").hide();
		doFirst = true;
	}
	
}
</script>
</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">首页</a></li>
    </ul>
    </div>
    
    <div >
    
    <div >
    
    	<ul  class="toolbar">
        <li class="click"><img src="css/images/t01.png" /><a href="SelectCourse_addPrompt.action" target="rightFrame"><span></span>添加</a></li>
          <li class="click"><img src="css/images/t03.png" /><input type="submit" value="删除" /></li>
        </ul>
        <!-- javascript:document.roleDetailForm.submit(); -->
    
    </div>
    
    
    <form id="selectCourseDetailForm" name="selectCourseDetailForm" action="SelectCourse_doBatchDelete.action" method="post">
     <table class="tablelist">
    	<thead>
    	<tr>
        <th style="width: 50px;"><input id="checkAll" name="" type="checkbox" value=""/></th>
        <th style="width: 50px;">编号</th>
        <th style="width: 150px;">所选任务名</th>       
        <th style="width: 150px;">所选课程设计名</th>

        <th style="width: 100px;">选课学生</th>
        <th style="width: 200px;">申请日期</th>
        <th onclick="reSort()" style="width: 70px;">状态<i class="sort"></i></th>
        <th style="width: 200px;">学生申请说明</th>
        <th style="width: 200px;">得分</th>
        <!-- <th>操作</th> -->
        </tr>
        </thead>
        <!-- 优先显示未检测的tbody -->
        <tbody  id="doNot_first">
        <c:forEach items="${page.datas}" var="SelectCourse">
        <c:if test="${SelectCourse.status eq '未审批'}">
        <tr>
        <td><input name="ids" type="checkbox" value="${SelectCourse.id}" /></td>
        <td>${SelectCourse.id}</td>
        <td>${SelectCourse.taskName}</td>       
        <td>${SelectCourse.projectName}</td>

        <td>${SelectCourse.selectName}</td>
        <td>${SelectCourse.createDate}</td>
        <td>${SelectCourse.status}</td>
        <td>${SelectCourse.desc}</td>
       <!-- <td><a href="SelectCourseApplication_updatePrompt.action?SelectCourseApplication.id=${SelectCourse.id}" class="tablelink">更新</a>   -->  
       	</td>
        </tr>
        </c:if>
        </c:forEach>
        
        <c:forEach items="${page.datas}" var="SelectCourse">
        <c:if test="${SelectCourse.status eq '已审批'}">
        <tr>
        <td><input name="ids" type="checkbox" after="true" value="${SelectCourse.id}" /></td>
        <td>${SelectCourse.id}</td>
        <td>${SelectCourse.taskName}</td>       
        <td>${SelectCourse.projectName}</td>

        <td>${SelectCourse.selectName}</td>
        <td>${SelectCourse.createDate}</td>
        <td>${SelectCourse.status}</td>
        <td>${SelectCourse.desc}</td>
        <td>${SelectCourse.score}</td>
       	</td>
        </tr>
        </c:if>
        </c:forEach>  
        </tbody>
        
        <!-- 优先显示已检测的tbody -->
        <tbody  id="do_first">
        <c:forEach items="${page.datas}" var="SelectCourse">
        <c:if test="${SelectCourse.status eq '已审批'}">
        <tr>
        <td><input name="ids" type="checkbox" value="${SelectCourse.id}" /></td>
        <td>${SelectCourse.id}</td>
        <td>${SelectCourse.taskName}</td>       
        <td>${SelectCourse.projectName}</td>
   
        <td>${SelectCourse.selectName}</td>
        <td>${SelectCourse.createDate}</td>
        <td>${SelectCourse.status}</td>
        <td>${SelectCourse.desc}</td>
        <td>${SelectCourse.score}</td>
        <!-- <td><a href="SelectCourseApplication_updatePrompt.action?SelectCourseApplication.id=${SelectCourse.id}" class="tablelink">更新</a>  -->    
       	</td>
        </tr>
        </c:if>
        </c:forEach>
        
        <c:forEach items="${page.datas}" var="SelectCourse">
        <c:if test="${SelectCourse.status eq '未审批'}">
        <tr>
        <td><input name="ids" type="checkbox" value="${SelectCourse.id}" /></td>
        <td>${SelectCourse.id}</td>
        <td>${SelectCourse.taskName}</td>       
        <td>${SelectCourse.projectName}</td>
       
        <td>${SelectCourse.selectName}</td>
        <td>${SelectCourse.createDate}</td>
        <td>${SelectCourse.status}</td>
        <td>${SelectCourse.desc}</td>
        <!-- <td><a href="SelectCourseApplication_updatePrompt.action?SelectCourseApplication.id=${SelectCourse.id}" class="tablelink">更新</a> -->    
       	</td>
        </tr>
        </c:if>
        </c:forEach>  
        </tbody>
    </table>
   
    </form>
    
     <div class="pagin">
    	<div class="message">共<i class="blue">${page.total}</i>条记录，当前显示第&nbsp;<i class="blue"><label>${page.pageNo}</label>&nbsp;</i>页</div>
		<ul class="paginList">
			<%@include file="/page/page.html" %>
		</ul>
    </div> 
    
        
    </div>    
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
</body>

</html>
