<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>给分</title>
<link href="css/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="css/editor/kindeditor.js"></script>
<script type="text/javascript">
    KE.show({
        id : 'content7',
        cssPath : './index.css'
    });
</script> 
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	
</head>

<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
    <div class="itab">
  	<ul> 
    <li><a href="#tab1" class="selected">给分</a></li> 
  	</ul>
    </div> 
    
    <form name="addRoleForm" action="SelectCourse_doUpdate.action?selectCourse.id=${updateSelectCourse.id}"  method="post">
  	<div id="tab1" class="tabson">
    
    <div class="formtext">Hi，<b>${user.userName}</b>，欢迎您试用修改用户功能！</div>
    
    <ul class="forminfo">
    <li><label>所选任务名<b>*</b></label><input id="taskName" name="selectCourse.taskName" type="text" class="dfinput" value="${updateSelectCourse.taskName}"  style="width:300px;"/></li>
   	<li><label>所选课程设计名<b>*</b></label><input id="projectName" name="selectCourse.projectName" type="text" class="dfinput" value="${updateSelectCourse.projectName}"  style="width:300px;"/></li>
 	<li><label>选课学生<b>*</b></label><input id="selectName" name="selectCourse.selectName" type="text" class="dfinput" value="${updateSelectCourse.selectName}"  style="width:250px;"/></li>
 	<li><label>申请日期<b>*</b></label><input id="createDate" name="selectCourse.createDate" type="text" class="dfinput" value="${updateSelectCourse.createDate}"  style="width:250px;"/></li>
 	<li><label>状态<b>*</b></label><input id="status" name="selectCourse.status" type="text" class="dfinput" value="${updateSelectCourse.status}"  style="width:250px;"/></li>
 	<li><label>学生申请说明<b>*</b></label><input id="desc" name="selectCourse.desc" type="text" class="dfinput" value="${updateSelectCourse.desc}"  style="width:250px;"/></li>
 	<li><label>得分<b>*</b></label><input id="content7" name="selectCourse.score" type="text" class="dfinput" value=""  style="width:800px;"/></li>
 	<li><label>&nbsp;</label><input type="submit" value="提交给分"></li>
 	</ul>
 	
	</form>
</body>
</html>
