<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<%=basePath%>">

<title>My JSP 'project_list.jsp' starting page</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<link href="css/css/style.css" rel="stylesheet" type="text/css" />

<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

</head>
<script type="text/javascript" src="js/jquery.js"></script>
<script>
	$(function() {
		var object = $('#projectName');
		$('#bt').click(function() {
			$('#t1').find("tr").remove();
			//location.href="Project_strlistProject?projectName="+object.val()+"";
			$.post("Project_strlistProject", {
				'projectName' : object.val()
			}, function(result) {
				for (var i = 0; i < result.rows.length; i++) {
					var endtime = '';
					if (result.rows[i].projectendtime != "" && result.rows[i].projectendtime != undefined) {
						var endtime = result.rows[i].projectendtime;
					}
					$('#t1').append("<tr><td>" + result.rows[i].projectName + "</td>" +
						"<td>" + result.rows[i].teacher + "</td>" +
						"<td>" + result.rows[i].projectcreatetime + "</td>" +
						"<td>" + endtime + "</td>" +
						"<td>" + result.rows[i].projectdesc + "</td>" +
						"<td>" + result.rows[i].projectstatus + "</td>" +
						"</tr>");
				}
			}, 'json');
		});
	});
</script>
<body>
	<div class="place">
		<span>位置：</span>
		<ul class="placeul">
			<li><a href="User_doLogin.action">首页内容</a></li>
		</ul>
	</div>
	<div class="place">
		<table class="tablelist">
			<thead>
				<tr>
					<th style="width: 200px;">课程设计名</th>
					<th style="width: 200px;">所属教师</th>
					<th style="width: 300px;">创建日期</th>
					<th style="width: 300px;">结束时间</th>
					<th style="width: 180px;">描述</th>
					<th style="width: 180px;">状态</th>

				</tr>
			</thead>
			<input type="text" id="projectName" name="projectName" />
			<img src="css/images/t02.png" />
			<button type="button" id="bt">查询</button>
			<tbody id="t1">
				<%-- <c:forEach items="${page.datas}" var="user">
					<tr>
						<td><input name="ids" type="checkbox" value="${user.id}" /></td>
						<td>${user.id}</td>
						<td>${user.userName}</td>
						<td>${user.createDate}</td>
						<td><c:forEach items="${user.user_role}" var="role">
								<c:out value="${role.roleDesc}"></c:out> &nbsp;&nbsp;
        	</c:forEach></td>
						<td><c:forEach items="${user.user_classroom}" var="classroom">
								<c:out value="${classroom.classroomName}"></c:out> &nbsp;&nbsp;
        	</c:forEach></td>
						<td><a
							href="User_updatePrompt.action?user.userName=${user.userName}">更新</a>
						</td>

					</tr>
				</c:forEach> --%>
			</tbody>
		</table>
	
	</div>
	</form>

</body>
</html>