<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'project_list.jsp' starting page</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

  </head>
  
  <body>
  
   	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="User_doLogin.action">首页内容</a></li>
    </ul>
    </div>
    <form id="projectDetailForm" name="projectDetailForm" action="Project_doBatchDelete.action" method="post">
     <table class="tablelist">
    	<thead>
    	<tr>
        <th style="width: 50px;"><input id="checkAll" name="" type="checkbox" value=""/></th>
        <th style="width: 80px;">编号</th>
        <th style="width: 80px;">课程设计名</th>       
        <th style="width: 80px;">所属教师</th>
        <th style="width: 80px;">创建日期</th>
        <th style="width: 80px;">结束时间</th>
        <th style="width: 80px;">描述</th>
        <th style="width: 80px;">状态</th>
        <th>操作</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${page.datas}" var="user">
        <tr>
        <td><input name="ids" type="checkbox" value="${user.id}" /></td>
        <td>${user.id}</td>
        <td>${user.userName}</td>       
        <td>${user.createDate}</td> 
        <td>
        	<c:forEach items="${user.user_role}" var="role">
        		<c:out value="${role.roleDesc}"></c:out> &nbsp;&nbsp;
        	</c:forEach>
        </td>   
         <td>
        	<c:forEach items="${user.user_classroom}" var="classroom">
        		<c:out value="${classroom.classroomName}"></c:out> &nbsp;&nbsp;
        	</c:forEach>
        </td>    
        <td><a href="User_updatePrompt.action?user.userName=${user.userName}">更新</a>   
       	</td>
        </tr> 
        </c:forEach>     
        </tbody>
    </table>
    <input type="submit" value="删除"/>
    
    
    
    </form>
           <form id="projectDetailForm" name="projectDetailForm" action="Project_doBatchDelete.action" method="post">
    <table class="tablelist">
    	<thead>
    	<tr>
        <th style="width: 50px;"><input id="checkAll" name="" type="checkbox" value=""/></th>
        <th style="width: 50px;">编号</th>
        <th style="width: 90px;">部门名</th>       
        <th style="width: 120px;">实验室名</th>
        <th style="width: 90px;">设备编号</th>
        <th style="width: 80px;">报修人</th>
        <th style="width: 80px;">审核人</th>
        <th style="width: 140px;">申请日期</th>
        <th style="width: 140px;">处理日期</th>
        <th onclick="reSort()" style="width: 70px;">状态<i class="sort"><img src="static/images/px.gif" /></i></th>
        <th style="width: 100px;">故障现象说明</th>
        <th style="width: 100px;">故障检测情况</th>
        <th>操作</th>
        </tr>
        </thead>
        <tbody id="doNot_first" >
        <c:forEach items="${page.datas}" var="repair">
        <c:if test="${repair.status eq '未检测'}">
        <tr>
        <td><input name="ids" type="checkbox" value="${repair.id}" /></td>
        <td>${repair.id}</td>
        <td>${repair.deptName}</td>       
        <td>${repair.labName}</td>
        <td>${repair.equipId}</td>
        <td>${repair.techName}</td>
        <td>${repair.inspName}</td>
        <td>${repair.createDate}</td>
        <td>${repair.dealDate}</td>
        <td>${repair.status}</td>
        <td>${repair.desc}</td>
        <td>${repair.sitn}</td>
        <td><a href="RepairApplication_examinePrompt.action?repairApplication.id=${repair.id}" class="tablelink">审核</a>     
       	</td>
        </tr> 
        </c:if>
        </c:forEach>
        
        <c:forEach items="${page.datas}" var="repair">
        <c:if test="${repair.status eq '已检测'}">
        <tr>
        <td><input name="ids" type="checkbox" after="true" value="${repair.id}" /></td>
        <td>${repair.id}</td>
        <td>${repair.deptName}</td>       
        <td>${repair.labName}</td>
        <td>${repair.equipId}</td>
        <td>${repair.techName}</td>
        <td>${repair.inspName}</td>
        <td>${repair.createDate}</td>
        <td>${repair.dealDate}</td>
        <td>${repair.status}</td>
        <td>${repair.desc}</td>
        <td>${repair.sitn}</td>
        <td><a href="RepairApplication_examinePrompt.action?repairApplication.id=${repair.id}" class="tablelink">审核</a>     
       	</td>
        </tr> 
        </c:if>
        </c:forEach>
        </tbody>
        
        
        
        <tbody id="do_first">
        <c:forEach items="${page.datas}" var="repair">
        <c:if test="${repair.status eq '已检测'}">
        <tr>
        <td><input name="ids" type="checkbox" value="${repair.id}" /></td>
        <td>${repair.id}</td>
        <td>${repair.deptName}</td>       
        <td>${repair.labName}</td>
        <td>${repair.equipId}</td>
        <td>${repair.techName}</td>
        <td>${repair.inspName}</td>
        <td>${repair.createDate}</td>
        <td>${repair.dealDate}</td>
        <td>${repair.status}</td>
        <td>${repair.desc}</td>
        <td>${repair.sitn}</td>
        <td><a href="RepairApplication_examinePrompt.action?repairApplication.id=${repair.id}" class="tablelink">审核</a>     
       	</td>
        </tr> 
        </c:if>
        </c:forEach>
        
        <c:forEach items="${page.datas}" var="repair">
        <c:if test="${repair.status eq '未检测'}">
        <tr>
        <td><input name="ids" type="checkbox" value="${repair.id}" /></td>
        <td>${repair.id}</td>
        <td>${repair.deptName}</td>       
        <td>${repair.labName}</td>
        <td>${repair.equipId}</td>
        <td>${repair.techName}</td>
        <td>${repair.inspName}</td>
        <td>${repair.createDate}</td>
        <td>${repair.dealDate}</td>
        <td>${repair.status}</td>
        <td>${repair.desc}</td>
        <td>${repair.sitn}</td>
        <td><a href="RepairApplication_examinePrompt.action?repairApplication.id=${repair.id}" class="tablelink">审核</a>     
       	</td>
        </tr> 
        </c:if>
        </c:forEach>
        </tbody>
        
    </table>
    </form>
    
  </body>
</html>
