﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<meta charset="utf-8">
<title>ECharts - Generate By @isea533/abel533</title>
<style>
* {
	margin: 0
}

html, body {
	height: 100%
}

.wrapper {
	min-height: 100%;
	height: auto !important;
	height: 100%;
	margin: 0 auto -155px
}

.footer, .push {
	height: 155px
}

table.gridtable {
	font-family: verdana, arial, sans-serif;
	font-size: 11px;
	color: #333;
	border-width: 1px;
	border-color: #666;
	border-collapse: collapse;
	margin: 5px auto
}

table.gridtable th {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666;
	background-color: #dedede
}

table.gridtable td {
	border-width: 1px;
	padding: 8px;
	border-style: solid;
	border-color: #666;
	background-color: #fff
}

.middle {
	text-align: center;
	margin: 0 auto;
	width: 90%;
	height: auto
}

.info {
	font-size: 12px;
	text-align: center;
	line-height: 20px;
	padding: 40px
}

.info a {
	margin: 0 10px;
	text-decoration: none;
	color: green
}
</style>
<!-- <script type="text/javascript" src="css/shoudong.js"></script> -->
<script type="text/javascript" src="js/jquery.js"></script>
<!-- ECharts单文件引入 -->
<script src="js/echarts.js"></script>
<!-- 修改主题 -->
 <script src="js/macarons.js"></script>

</head>
<body>
	<div class="wrapper">
		<h2 style="padding: 10px 10px 80px 20px;">欢迎使用管理系统</h2>
		<sec:authorize ifAnyGranted="ROLE_TEACHER">
			<div class="middle">

				<!-- 为ECharts准备一个具备大小（宽高）的Dom -->

				<div id="main" style="height:400px"></div>
				
			</div>
		</sec:authorize>
		<div class="push"></div>

	</div>
	<div class="footer"></div>
</body>



<script type="text/javascript">
window.onload=function(){
	$.ajax({  
	    type:"post",  
	    url:"chart",  
	    dataType:"json",  
	    async:false,  
	    success:function(data){  
	    	var option = eval("("+data+")");
	    	alert(data);
	    }  
	}); 
	};
</script> 
<script type="text/javascript" src="css/shoudong.js">
				/* 	var myChart = echarts.init(document.getElementById('main'), theme);
				
					option = {
						title : {
							text : '统计选课审批人数',
							subtext : '纯属虚构'
						},
						tooltip : {
							trigger : 'axis'
						},
						legend : {
							data : [ '已审批', '未审批' ]
						},
						toolbox : {
							show : true,
							feature : {
								mark : {
									show : true
								},
								dataView : {
									show : true,
									readOnly : false
								},
								magicType : {
									show : true,
									type : [ 'line', 'bar' ]
								},
								restore : {
									show : true
								},
								saveAsImage : {
									show : true
								}
							}
						},
						calculable : true,
						xAxis : [
							{
								type : 'category',
								data : [ '课程设计', '数据库课程设计' ]
							}
						],
						yAxis : [
							{
								type : 'value'
							}
						],
						series : [
							{
								name : '已审批',
								type : 'bar',
								data : [ '2', '1' ],
								markPoint : {
									data : [
										{
											type : 'max',
											name : '最大值'
										},
										{
											type : 'min',
											name : '最小值'
										}
									]
								},
								markLine : {
									data : [
										{
											type : 'average',
											name : '平均值'
										}
									]
								}
							},
							{
								name : '未审批',
								type : 'bar',
								data : [ '1', '7' ],
								markPoint : {
									data : []
								},
								markLine : {
									data : [
										{
											type : 'average',
											name : '平均值'
										}
									]
								}
							}
						]
					};
					myChart.setOption(option); */
				</script>
</html>
