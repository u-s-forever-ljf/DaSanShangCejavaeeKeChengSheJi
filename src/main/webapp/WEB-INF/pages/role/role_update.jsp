<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>更新角色</title>
<link href="css/css/style.css" rel="stylesheet" type="text/css" />
<link href="css/css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="css/editor/kindeditor.js"></script>
<script type="text/javascript">
    KE.show({
        id : 'content7',
        cssPath : './index.css'
    });
</script> 
  
<script type="text/javascript">
$(document).ready(function(e) {
    $(".select1").uedSelect({
		width : 345			  
	});
	$(".select2").uedSelect({
		width : 167  
	});
	$(".select3").uedSelect({
		width : 100
	});
});
</script>
<script type="text/javascript">
	//验证角色名是否为空
	$(function() {
		$('#createRole').click(function() {
			var roleName = $('#roleName').val();
			if (roleName == '') {
				alert('角色名不能为空！');
				return false;
			}else {
				$('#addRoleForm').submit();
			}
		});
	});
</script>
</head>

<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="PageFrame_index.action">首页</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    
    <div id="usual1" class="usual"> 
    
    <div class="itab">
  	<ul> 
    <li><a href="#tab1" class="selected">更新角色</a></li> 
  	</ul>
    </div> 
    
    <form name="addRoleForm" action="Role_doUpdate" method="post">
  	<div id="tab1" class="tabson">
    
    <div class="formtext">Hi，<b>${user.userName}</b>，欢迎您试用更新角色功能！</div>
    
    <ul class="forminfo">
    <li><label>角色名称<b>*</b></label><input id="roleName" name="role.roleName" type="text" class="dfinput" value="${updateRole.roleName}"  style="width:250px;"/></li>
   	
 
    <li><label>描述<b>*</b></label>
    <textarea id="content7" name="role.roleDesc" style="width:700px;height:250px;visibility:hidden;">${updateRole.roleDesc}</textarea>  
    </li>
    
    <li><input type="hidden" name="role.id" type="text" class="dfinput" value="${updateRole.id}"  style="width:250px;"/></li>
   	<li><input type="hidden" name="role.createDate" type="text" class="dfinput" value="${updateRole.createDate}"  style="width:250px;"/></li>
   	<li><input type="hidden" name="role.createUserName" type="text" class="dfinput" value="${updateRole.createUserName}"  style="width:250px;"/></li>
   	
    <li><label>&nbsp;</label><input id="createRole" name="" type="submit" class="btn" value="马上更新"/></li>
    </ul>
    
    </div> 
    </form>
    
	</div> 
 
	<script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
    
    
    </div>


</body>

</html>
