﻿﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/css/style.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="css/js/jquery.js"></script>

<script type="text/javascript">
	$(function() {
		//导航切换
		$(".menuson li").click(function() {
			$(".menuson li.active").removeClass("active");
			$(this).addClass("active");
		});

		$('.title').click(function() {
			var $ul = $(this).next('ul');
			$('dd').find('ul').slideUp();
			if ($ul.is(':visible')) {
				$(this).next('ul').slideUp();
			} else {
				$(this).next('ul').slideDown();
			}
		});
	});
</script>


</head>

<body style="background:#f0f9fd;">
	<div class="lefttop">
		<span></span>通讯录
	</div>

	<dl class="leftmenu">
		<!-- 权限标签 -->
		<!-- ifAllGranted，只有当前用户同时拥有 ROLE_ADMIN 和 ROLE_USER 两个权限时，才能显示标签内部内容 -->
		<!-- ifAnyGranted，如果当前用户拥有 ROLE_ADMIN 或 ROLE_USER 其中一个权限时，就能显示标签内部内容 -->
		<!-- ifNotGranted，如果当前用户没有 ROLE_ADMIN 时，才能显示标签内部内容 -->
		<dd>
			<div class="title">
				<span><img src="css/images/leftico01.png" /></span>基本信息
			</div>
			<ul class="menuson">
				<li class="active"><cite></cite><a
					href="PageFrame_index.action" target="rightFrame">首页</a><i></i></li>

				<sec:authorize ifAnyGranted="ROLE_TEACHER,ROLE_ADMIN">
					<li><cite></cite><a href="Role_listPrompt.action"
						target="rightFrame">角色列表</a><i></i></li>
					<li><cite></cite><a href="User_listPrompt.action"
						target="rightFrame">用户列表</a><i></i></li>
					<li><cite></cite><a href="Classroom_listPrompt.action"
						target="rightFrame">班级列表</a><i></i></li>
				</sec:authorize>


			</ul>
		</dd>

		<sec:authorize ifAnyGranted="ROLE_TEACHER">
			<dd>
				<div class="title">
					<span><img src="css/images/leftico03.png" /></span>课程设计管理
				</div>
				<ul class="menuson">
					<sec:authorize ifAnyGranted="ROLE_TEACHER,ROLE_STUDENT">
						<li><cite></cite><a
							href="SelectCourse_searchTaskByProject.action"
							target="rightFrame">组合查询查任务</a><i></i></li>
						<li><cite></cite><a href="SelectCourse_listPrompt.action"
							target="rightFrame">课设情况列表</a><i></i></li>
						<li><cite></cite><a href="SelectCourse_addPrompt.action"
							target="rightFrame">选课列表</a><i></i></li>
					</sec:authorize>
					<sec:authorize ifAnyGranted="ROLE_TEACHER">
						<li><cite></cite><a
							href="SelectCourse_approvallistPrompt.action" target="rightFrame">审核列表</a><i></i></li>
						<li><cite></cite><a
							href="SelectCourse_approvallistPrompt.action" target="rightFrame">打分</a><i></i></li>
					</sec:authorize>
					<sec:authorize ifAnyGranted="ROLE_TEACHER">
						<!-- <a href="SelectCourse_findTask?projectId=2"target="rightFrame"> 测试查任务projectId=2返回json</a></br> -->
						<li><cite></cite><a href="Project_listProject.action"
							target="rightFrame">查询课题</a><i></i></li>
					</sec:authorize>


					<sec:authorize ifAnyGranted="ROLE_TEACHER">
						<li><cite></cite><a href="Project_addPrompt.action"
							target="rightFrame">发布课程设计</a><i></i></li>
						<li><cite></cite><a href="Task_addPrompt.action"
							target="rightFrame">发布任务</a><i></i></li>
					</sec:authorize>

				</ul>
			</dd>
		</sec:authorize>
		<sec:authorize ifAnyGranted="ROLE_STUDENT">
			<dd>
				<div class="title">
					<span><img src="css/images/leftico03.png" /></span>课程设计管理
				</div>
				<ul class="menuson">
					<div>
						<li><cite></cite><a href="Project_listProject.action"
							target="rightFrame">查询课题</a><i></i></li>
						<li><cite></cite><a
							href="SelectCourse_searchTaskByProject.action"
							target="rightFrame">组合查询查任务</a><i></i></li>
						<li><cite></cite><a href="SelectCourse_listPrompt.action"
							target="rightFrame">课设情况列表</a><i></i></li>
						<li><cite></cite><a href="SelectCourse_addPrompt.action"
							target="rightFrame">选课列表</a><i></i></li>
				</ul>
				</div>
			</dd>
		</sec:authorize>
		<sec:authorize ifAnyGranted="ROLE_ADMIN">
			<dd>
				<div>
					<a href="Log_listPrompt.action" target="rightFrame"><span><img
							src="css/images/leftico02.png" /></span>系统日志</a>
				</div>
			</dd>
		</sec:authorize>

	</dl>

</body>
</html>
