<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/css/style.css" rel="stylesheet" type="text/css" />
<title>更新用户</title>



</head>

<body>

	<div class="place">
		<span>位置：</span>
		<ul class="placeul">
		</ul>
	</div>

	<div class="formbody">


		<div id="usual1" class="usual">

			<div class="itab">
				<ul>
					<li><a href="#tab1" class="selected">修改用户</a></li>
				</ul>
			</div>

			<form name="addRoleForm"
				action="User_doUpdate.action?user.id=${user.id}"
				onsubmit="return checkForm()" method="post">
				<div id="tab1" class="tabson">

					<div class="formtext">
						Hi，<b>${user.userName}</b>，欢迎您试用修改用户功能！
					</div>

					<ul class="forminfo">
						<li><label>用户名称<b>*</b></label><input id="userName"
							name="user.userName" type="text" class="dfinput"
							value="${requestScope.user.userName}" style="width:250px;" /></li>
						<li><label>密 码<b>*</b></label><input id="password"
							name="user.userPassword" type="password" class="dfinput"
							value="${requestScope.user.userPassword}" style="width:250px;" /></li>
						<li><label>确认密码<b>*</b></label><input id="rePassword"
							name="rePassword" type="password" class="dfinput"
							value="${requestScope.user.userPassword}" style="width:250px;" /></li>
						<li><label>联系电话<b>*</b></label><input id="userTelephone"
							name="userTelephone" type="text" class="dfinput"
							value="${requestScope.user.userTelephone}" style="width:250px;" /></li>
						<li><label>用户地址<b>*</b></label><input id="userAddress"
							name="userAddress" type="text" class="dfinput"
							value="${requestScope.user.userAddress}" style="width:250px;" /></li>
						<li><label>用户邮箱<b>*</b></label><input id="userEmail"
							name="userEmail" type="text" class="dfinput"
							value="${requestScope.user.userEmail}" style="width:250px;" /></li>
						<li><label>真实姓名<b>*</b></label><input id="userRealName"
							name="userRealName" type="text" class="dfinput"
							value="${requestScope.user.userRealName}" style="width:250px;" /></li>

						<li><label>角色<b>*</b></label> <!-- Set必须要使用makeNew运算符，这样ognl才能帮我创建新的对象，否则roles会为空 。
    	如果集合类型是List，直接name="roles[0].id"就可以。
    	在Class-conversion.properties文件中加上KeyProperty_orders=id，这是封装Set时必须的
     -->
							<table>
								<tbody>
									<c:set var="i" value="0" />
									<c:forEach items="${requestScope.user.user_role}" var="role"
										varStatus="status">
										<td><input name="roles.makeNew[${i}].id"
											checked="checked" checkboxType='role' type="checkbox"
											value="${role.id}" /></td>
										<td>&nbsp;${role.roleDesc}&nbsp;&nbsp;</td>
										<c:set var="i" value="${i+1}" />
									</c:forEach>

									<c:forEach items="${list}" var="role" varStatus="status">
										<td><input name="roles.makeNew[${i}].id" type="checkbox"
											checkboxType='role' value="${role.id}" /></td>
										<td>&nbsp;${role.roleDesc}&nbsp;&nbsp;</td>
										<c:set var="i" value="${i+1}" />
									</c:forEach>
								</tbody>
							</table></li>
						<label>班级<b>*</b></label>
						<!-- Set必须要使用makeNew运算符，这样ognl才能帮我创建新的对象，否则roles会为空 。
    	如果集合类型是List，直接name="roles[0].id"就可以。
    	在Class-conversion.properties文件中加上KeyProperty_orders=id，这是封装Set时必须的
     -->
						<table>
							<tbody>
								<c:set var="i" value="0" />
								<c:forEach items="${requestScope.user.user_classroom}"
									var="classroom" varStatus="status">
									<td><input name="classrooms.makeNew[${i}].id"
										checked="checked" checkboxType='classroom' type="checkbox"
										value="${classroom.id}" /></td>
									<td>&nbsp;${classroom.classroomName}&nbsp;&nbsp;</td>
									<c:set var="i" value="${i+1}" />
								</c:forEach>

								<c:forEach items="${classroomList}" var="classroom"
									varStatus="status">
									<td><input name="classrooms.makeNew[${i}].id"
										type="checkbox" checkboxType='classroom'
										value="${classroom.id}" /></td>
									<td>&nbsp;${classroom.classroomName}&nbsp;&nbsp;</td>
									<c:set var="i" value="${i+1}" />
								</c:forEach>
							</tbody>
						</table>
						</li>




						<li><label>&nbsp;</label><input type="submit" value="提交修改"></li>
					</ul>

				</div>
			</form>

		</div>




	</div>


</body>

</html>
