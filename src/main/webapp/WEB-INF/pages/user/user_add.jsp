<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="css/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>
<title>新增user</title>

<script type="text/javascript">
	$(document).ready(function(e) {
		var msg = '${requestScope.msg}';
		if (msg != null && msg != '') {
			alert(msg);
		}

		$(".dfinput").each(function() {
			$(this).change(function() {
				check();
			});
		});

		var result = '${result.msg}';
		if ('' !== result) {
			alert(result);
		}

		function check() {
			var dfinput = $(".dfinput");
			for (var i = 0; i < dfinput.length; i++) {
				var val = $(dfinput[i]).val();
				if (!val) {
					$("#submit_but").css("background", "rgb(169,169,169)");
					return false;
				}
			}
			$("#submit_but").removeAttr("disabled");
			$("#submit_but").css("background", "rgb(62,175,224)");
		}

		$("#userName").focusout(function() {
			$.ajax({
				url : 'User_checkName.action',
				type : 'POST', //GET
				async : false, //或false,是否异步
				data : {
					'user.userName' : $("#userName").val()
				},
				dataType : 'json', //返回的数据格式：json/xml/html/script/jsonp/text
				success : function(data) {
					if (data.code == 0) {
						alert(data.msg);
					}

				},
				error : function(XMLHttpRequest, textStatus, errorThrown) {}
			});
		});

	});
	function checkForm() {
		if ($("#password").val() != $("#rePassword").val()) {
			alert("密码与确认密码不符");
			return false;
		}

		var len = $("input[checkboxType=role]:checkbox:checked").length;
		if (len == 0) {
			alert("至少选择一个角色！");
			return false;
		}



		return true;
	}
</script>

</head>

<body>




	<div class="itab">
		<ul>
		</ul>
	</div>

	<form name="addRoleForm" action="User_addUser.action"
		onsubmit="return checkForm()" method="post">
		<div>

			<div>
				Hi，<b>${user.userName}</b>，欢迎您试用新增用户功能！
			</div>

			<ul>
				<li><label>用户名称<b>*</b></label><input id="userName"
					name="user.userName" type="text" class="dfinput" value=""
					style="width:250px;" /></li>
				<li><label>密 码<b>*</b></label><input id="password"
					name="user.userPassword" type="password" class="dfinput" value=""
					style="width:250px;" /></li>
				<li><label>确认密码<b>*</b></label><input id="rePassword"
					name="rePassword" type="password" class="dfinput" value=""
					style="width:250px;" /></li>
				<!-- 使用正则表达式限制用户只能输入数字 -->
				<li><label>联系电话<b>*</b></label><input name="user.userTelephone"
					type="text" onkeyup="this.value=this.value.replace(/[^0-9-]+/,'');"
					maxlength="11" class="dfinput" value="" style="width:250px;" /></li>
				<li><label>用户地址<b>*</b></label><input id="userAddress"
					name="user.userAddress" type="text" class="" value=""
					style="width:250px;" /></li>

				<li><label>用户邮箱<b>*</b></label><input id="userEmail"
					name="user.userEmail" type="text" class="" value=""
					style="width:250px;" /></li>
				<li><label>真实姓名<b>*</b></label><input id="userRealName"
					name="user.userRealName" type="text" class="" value=""
					style="width:250px;" /></li>

				<li><label>角色<b>*</b></label> <!-- Set必须要使用makeNew运算符，这样ognl才能帮我创建新的对象，否则roles会为空 。
    	如果集合类型是List，直接name="roles[0].id"就可以。
    	在Class-conversion.properties文件中加上KeyProperty_orders=id，这是封装Set时必须的
     -->
					<table>
						<tbody>
							<tr style="height: 34px">
								<c:forEach items="${list}" var="role" varStatus="status">
									<td><input name="roles.makeNew[${status.index}].id"
										checkboxType='role' type="checkbox" value="${role.id}" /></td>
									<td>&nbsp;${role.roleDesc}&nbsp;&nbsp;</td>
								</c:forEach>
							</tr>
						</tbody>

					</table></li>
				<li>

					<li><label>班级<b>*</b></label> <!-- Set必须要使用makeNew运算符，这样ognl才能帮我创建新的对象，否则roles会为空 。
    	如果集合类型是List，直接name="roles[0].id"就可以。
    	在Class-conversion.properties文件中加上KeyProperty_orders=id，这是封装Set时必须的
     -->
						<table>
							<tbody>
								<tr style="height: 34px">
									<c:forEach items="${classroomList}" var="classroom"
										varStatus="status">
										<td><input name="classrooms.makeNew[${status.index}].id"
											checkboxType='classroom' type="checkbox"
											value="${classroom.id}" /></td>
										<td>&nbsp;${classroom.classroomName}&nbsp;&nbsp;</td>
									</c:forEach>
								</tr>
							</tbody>

						</table></li>

					<li><label>&nbsp;</label><input name="" id="submit_but"
						type="submit" disabled="disabled" class="btn"
						style="background: rgb(169,169,169)" value="马上创建" /></li>
			</ul>
		</div>
	</form>




</body>

</html>
