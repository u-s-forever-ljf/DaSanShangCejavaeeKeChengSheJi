﻿﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>无标题文档</title>
<link href="css/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<!-- 全选/全不选 -->

<script type="text/javascript">

	$(function() {
		$("#checkAll").click(function() {

			$('input[name="ids"]').attr("checked", this.checked);
		});
		var $ids = $("input[name='ids']");
		$ids.click(function() {
			$("checkAll").attr("checked", $ids.length == $("input[name='ids']:checked").length ? true : false);
		});
	});
</script>


</head>



<body>
	<div class="tools">

		<ul class="toolbar">
			<li class="click"><img src="css/images/t01.png" /><a
				href="User_toAdd.action" target="rightFrame">添加</a></li>
		</ul>

		<form id="userDetailForm" name="userDetailForm"
			action="User_doBatchDelete.action" method="post">

			<table class="tablelist">
				<thead>
					<tr>
						<th style="width: 50px;"><input id="checkAll" name=""
							type="checkbox" value="" /></th>
						<th style="width: 50px;">编号</th>
						<th style="width: 80px;">用户名</th>
						<th style="width: 80px;">真实姓名</th>
						<th style="width: 80px;">创建时间</th>
						<th style="width: 80px;">联系电话</th>
						<th style="width: 80px;">用户地址</th>
						<th style="width: 80px;">用户邮箱</th>
						<th style="width: 60px;">所属角色</th>
						<th style="width: 60px;">所属班级</th>
						<th style="width: 50px;">操作</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${page.datas}" var="user">
						<tr>
							<td><input name="ids" type="checkbox" value="${user.id}" /></td>
							<td>${user.id}</td>
							<td>${user.userName}</td>
							<td>${user.userRealName}</td>
							<td>${user.createDate}</td>
							<td>${user.userTelephone}</td>
							<td>${user.userAddress}</td>
							<td>${user.userEmail}</td>
							<td><c:forEach items="${user.user_role}" var="role">
									<c:out value="${role.roleDesc}"></c:out> &nbsp;&nbsp;
        	</c:forEach></td>
							<td><c:forEach items="${user.user_classroom}"
									var="classroom">
									<c:out value="${classroom.classroomName}"></c:out> &nbsp;&nbsp;
        	</c:forEach></td>
							<td><a
								href="User_updatePrompt.action?user.userName=${user.userName}">更新</a>
							</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
			<input type="submit" value="删除" />
		</form>


		<div class="pagin">
			<div class="message">
				共<i class="blue">${page.total}</i>条记录，当前显示第&nbsp;<i class="blue"><label>${page.pageNo}</label>&nbsp;</i>页
			</div>
			<ul class="paginList">
				<%@include file="/page/page.html"%>
			</ul>
		</div>


	</div>

	<script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
</body>

</html>
