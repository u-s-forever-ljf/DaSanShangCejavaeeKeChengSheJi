﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>系统日志</title>
<link href="css/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="css/js/jquery.js"></script>


</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="User_doLogin.action">首页内容</a></li>
    </ul>
    </div>
    
    <div class="rightinfo">
    
    <table class="tablelist">
    	<thead>
    	<tr>
        <th style="width: 50px;">编号</th>
        <th style="width: 90px;">用户名</th> 
        <th style="width: 100px;">IP地址</th>      
        <th style="width: 120px;">操作</th>
        <th style="width: 90px;">方法</th>
        <th>类名</th>
        <th style="width: 140px;">创建日期</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${page.datas}" var="log">
        <tr>
        <td>${log.id}</td>
        <td>${log.username}</td>       
        <td>${log.ipAdd}</td>
        <td>${log.msg}</td>
        <td>${log.method}</td>
        <td>${log.clazz}</td>
        <td>${log.createDate}</td>
        </tr> 
        </c:forEach>     
        </tbody>
    </table>
    
     <div class="pagin">
    	<div class="message">共<i class="blue">${page.total}</i>条记录，当前显示第&nbsp;<i class="blue"><label>${page.pageNo}</label>&nbsp;</i>页</div>
		<ul class="paginList">
			<%@include file="/page/page.html" %>
		</ul>
    </div> 
    
        
    </div>    
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>

</body>

</html>
