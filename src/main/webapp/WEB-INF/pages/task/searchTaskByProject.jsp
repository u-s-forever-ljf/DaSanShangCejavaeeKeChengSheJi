<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="/struts-tags" prefix="s"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>选课申请</title>
<link href="css/css/style.css" rel="stylesheet" type="text/css" />
<link href="css/css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jquery.idTabs.min.js"></script>
<script type="text/javascript" src="js/select-ui.min.js"></script>
<script type="text/javascript">
	$(document).ready(function(e) {
		$(".select1").uedSelect({
			width : 345
		});
		$(".select2").uedSelect({
			width : 180
		});
		$(".select3").uedSelect({
			width : 200
		});
	});
</script>
<script type="text/javascript">
	//页面加载时首先获得课程设计
	$(function() {
		$.ajax({
			type : "post",
			url : "project",
			dataType : "json",
			async : false,
			success : function(data) {
				//alert(data);
				var pro = eval("(" + data + ")");
				for (var i = 0; i < pro.length; i++) {
					$("#project").append("<option value=" + pro[i].id + ">" + pro[i].projectName + "</option>");
				}
			}
		});
	});
</script>
</head>
<body>
	<div class="place">
		<span>位置：</span>
		<ul class="placeul">
			<li><a href="PageFrame_index.action">首页</a></li>
		</ul>
	</div>
	<div class="formbody">
		<div id="usual1" class="usual">
			<div class="itab">
				<ul>
					<li><a href="" class="">组合查询任务表</a></li>
				</ul>
			</div>

			<form name="" action="" method="post">
				<div id="tab1">

					<div class="formtext">
						Hi，<b>${user.userName}</b>，欢迎您试用组合查询任务！
					</div>
					<!-- 三级联动参考   http://blog.csdn.net/luckey_zh/article/details/22995389 -->
					<ul class="forminfo">
						<li><label>课程设计<b>*</b></label><img src="css/images/d02.png" />
							<div class="vocation">
								<!-- 如果使用 select1 可能导致取不到值-->
								<select id="project" name="project" class="select3">
									<option value="0">--请选择--</option>
								</select>
							</div></li>
						<table class="tablelist">
							<thead>
								<tr>
									<th style="width: 50px;">编号</th>
									<th style="width: 90px;">创建时间</th>
									<th style="width: 100px;">处理时间</th>
									<th style="width: 120px;">发布人</th>
									<th style="width: 90px;">任务描述</th>
									<th style="width: 90px;">任务名称</th>
								</tr>
							</thead>
							<tbody>
							
					        	<th id="taskId" name="taskId"></th>
					        	<th id="createDate" name="createDate"></th>
					        	<th id="endDate" name="endDate"></th>
					        	<th id="taskByteacher" name="taskByteacher"></th>
					        	<th id="taskDesc" name="taskDesc"></th>
					        	<th id="taskName" name="taskName"></th>
					       
							</tbody>


						</table>

						<%-- <li id=""><label>任务</label> <select name="task" id="task"
							class="select3">
								<option selected="selected" value="">任务：</option>
								<s:iterator value="taskId">
									<option value="${taskId}">${taskName}</option>
								</s:iterator>
						</select></li>
						<li><input type="hidden" id="taskprojectId"
							name="taskprojectId" value="" /> --%>
					</ul>
				</div>
			</form>
		</div>
		<script type="text/javascript">
			$(function() {
				$("#project").change(function() {
					var url = "SelectCourse_findTask.action";
					var projectId = $("#project").val();
					$("#taskId option:not(:first)").remove();
					$.post(url, {
						"projectId" : projectId
					}, function(data) {
						/* alert("运行到2");
						alert(data); */
						var result = data.result;
						/* $("#task").attr("value", result); */
							for (var i = 0; i < result.length; i++) {
							$("#taskId").append( result[i].taskId );
							$("#createDate").append( result[i].createDate );
							$("#endDate").append( result[i].endDate );
							$("#taskByteacher").append( result[i].taskByteacher );
							$("#taskDesc").append( result[i].taskDesc );
							$("#taskName").append( result[i].taskName );
						}
						
					}, "json");
		
				});
			});
		</script>
<%-- 		<script type="text/javascript">
			$("#task").change(function() {
				$("#taskprojectId").attr("value", $("#task").val());
			});
			$("#project").change(function() {
				$("#projectId").attr("value", $("#project").val());
			});
		</script> --%>


		<script type="text/javascript">
			$("#usual1 ul").idTabs();
		</script>

		<script type="text/javascript">
			$('.tablelist tbody tr:odd').addClass('odd');
		</script>




	</div>


</body>

</html>
