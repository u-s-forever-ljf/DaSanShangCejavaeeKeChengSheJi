<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>新增任务</title>
<link href="css/css/style.css" rel="stylesheet" type="text/css" />
<link href="css/css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="css/editor/kindeditor.js"></script>
<script type="text/javascript">
    KE.show({
        id : 'content7',
        cssPath : './index.css'
    });
</script> 
<script type="text/javascript">
	window.onload=function(){
		$("#ldi").attr("value", $("#projectList").val());
		$("#projectList").change(function(){
			$("#ldi").attr("value", $("#projectList").val());
		});
		
	};
</script>

</body>



<form name="addTaskForm" action="Task_addTask.action" method="post">
	<div>

		<div>
			Hi，<b>${user.userName}</b>，欢迎您试用新增任务功能！
		</div>

		<ul>
			<li><label>任务名<b>*</b></label><input id="taskName"
				name="task.taskName" type="text" class="dfinput" value=""
				style="width:250px;" /></li>
			<li><label>所属教师<b>*</b></label><input id="taskByteacher"
				name="task.taskByteacher" type="text" class="dfinput"
				value="${user.userName}" style="width:250px;" /></li>
			<li><label>创建日期<b>*</b></label><input id="createDate"
				name="task.createDate" type="text" class="dfinput" value=""
				style="width:250px;" /></li>
			<li><label>结束时间<b>*</b></label><input id="endDate"
				name="task.endDate" type="text" class="dfinput" value=""
				style="width:250px;" /></li>

			<li><label>所属课程设计<b>*</b></label>
				<div class="vocation">
					<!-- 如果使用 select1 可能导致取不到值-->
					<select id="projectList">
						<c:forEach items="${projects}" var="project">
							<option value="${project.id}">${project.projectName}</option>
						</c:forEach>
					</select>
				</div></li>
			<li><input type="hidden" id="ldi" name="task.project.id"
				value="" /></li>
			<li><label>描述<b>*</b></label><input id="content7"
				name="task.taskDesc" type="text" class="dfinput" value=""
				style="width:500px;" /></li>
			<li><label>&nbsp;</label><input type="submit" value="提交发布"></li>
		</ul>
	</div>
</form>


</body>
</html>
