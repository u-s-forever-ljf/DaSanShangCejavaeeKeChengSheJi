<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>新增班级</title>
<link href="css/css/style.css" rel="stylesheet" type="text/css" />
<link href="css/css/select.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>





<script type="text/javascript">
$(document).ready(function(e) {
    $(".select1").uedSelect({
		width : 345			  
	});
	$(".select2").uedSelect({
		width : 167  
	});
	$(".select3").uedSelect({
		width : 100
	});
});
</script>
<script type="text/javascript">
	//验证角色名是否为空
	$(function() {
		$('#createClassroom').click(function() {
			var classroomName = $('#classroomName').val();
			if (classroomName == '') {
				alert('班级不能为空！');
				return false;
			}else {
				$('#addClassroomForm').submit();
			}
		});
	});
</script>
</head>

<body>

	<div class="place">
		<span>位置：</span>
		<ul class="placeul">
			<li><a href="PageFrame_index.action">首页</a></li>
		</ul>
	</div>

	<div class="formbody">


		<div id="usual1" class="usual">

			<div class="itab">
				<ul>
					<li><a href="#tab1" class="selected">新增班级</a></li>
				</ul>
			</div>

			<form name="addClassroomForm" action="Classroom_doSave" method="post">
				<div id="tab1" class="tabson">

					<div class="formtext">
						Hi，<b>${user.userName}</b>，欢迎您试用新增班级功能！
					</div>

					<ul class="forminfo">
						<li><label>班级名称<b>*</b></label> <input id="classroomName"
							name="classroom.classroomName" type="text" class="dfinput"
							value="" style="width:250px;" /> <img src="css/images/d02.png" />
						</li>
						<li><label>&nbsp;</label><input id="createClassroom" name=""
							type="submit" class="btn" value="马上创建" /></li>
					</ul>

				</div>
			</form>

		</div>

		<script type="text/javascript"> 
      $("#usual1 ul").idTabs(); 
    </script>

		<script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
	</div>


</body>

</html>
