﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<%=basePath%>" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>无标题文档</title>
<link href="css/css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery.js"></script>

<script type="text/javascript">
$(document).ready(function(){
  $(".click").click(function(){
  $(".tip").fadeIn(200);
  });
  
  $(".tiptop a").click(function(){
  $(".tip").fadeOut(200);
});

  $(".sure").click(function(){
  $(".tip").fadeOut(100);
});

  $(".cancel").click(function(){
  $(".tip").fadeOut(100);
});

});
</script>
<script type="text/javascript">
$(function() {
	//jQuery提交  
	$("#del").click(function(){
		   if($("input[name='ids']:checked").length == 0){
			   alert("至少选择一个班级！");
			   return;
	}
		   var gnl=confirm("确定要删除?");
		   if (gnl==true){
			 $("#classroomDetailForm").submit(); 
		     return true;
		   }else{
		     return false;
		   }
	    // 
	}); 
});
</script>
<!-- 全选/全不选 -->
<script type="text/javascript">
   $(function() {
           $("#checkAll").click(function() {
                $('input[name="ids"]').attr("checked",this.checked); 
            });
            var $ids = $("input[name='ids']");
            $ids.click(function(){
                $("#checkAll").attr("checked",$ids.length == $("input[name='ids']:checked").length ? true : false);
            });
    });
 </script>
</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">首页</a></li>
  
    </ul>
    </div>
    
    <div >
    
    <div >
    
    	<ul class="toolbar">
        <li class="click"><img src="css/images/t01.png" /><a href="Classroom_addPrompt.action" target="rightFrame">添加</a></li>
       <li class="click"> <img src="css/images/t03.png" /><a id="del"></span>删除</a></li>
        </ul>
        <!-- javascript:document.classroomDetailForm.submit(); -->
    
    </div>
    
    
    <form id="classroomDetailForm" name="classroomDetailForm" action="Classroom_doBatchDelete.action" method="post">
    <table class="tablelist">
    	<thead>
    	<tr>
        <th style="width: 50px;"><input id="checkAll" name="" type="checkbox" value=""/></th>
        <th style="width: 50px;">编号</th>
        <th style="width: 120px;">班级名称</th>   
        <th style="width: 120px;">创建时间</th>      
        <th style="width: 120px;">创建人</th>          
        <th style="width: 120px;">操作</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${page.datas}" var="classroom">
        <tr>
        <td><input name="ids" type="checkbox" value="${classroom.id}" /></td>
        <td>${classroom.id}</td>
        <td>${classroom.classroomName}</td>       
        <td>${classroom.createDate}</td>
        <td>${classroom.createUserName}</td>
        <td><a href="Classroom_updatePrompt.action?classroom.id=${classroom.id}" >更新</a>     
       	</td>
        </tr> 
        </c:forEach>     
        </tbody>
    </table>
    </form>
    
   
     <div class="pagin">
    	<div class="message">共<i class="blue">${page.total}</i>条记录，当前显示第&nbsp;<i class="blue"><label>${page.pageNo}</label>&nbsp;</i>页</div>
		<ul class="paginList">
			<%@include file="/page/page.html" %>
		</ul>
    </div>   
    
    </div>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>

</body>

</html>
