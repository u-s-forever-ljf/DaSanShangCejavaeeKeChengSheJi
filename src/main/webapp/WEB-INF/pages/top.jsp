﻿<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<base href="<%=basePath%>"/>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="css/css/style.css" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="css/js/jquery.js"></script>
<script type="text/javascript">
$(function(){	
	//顶部导航切换
	$(".nav li a").click(function(){
		$(".nav li a.selected").removeClass("selected");
		$(this).addClass("selected");
	});
});
</script>


</head>

<body style="background:url(css/images/topbg.gif) repeat-x;">

    <div class="topleft">
    <a href="PageFrame_main.action" target="_parent"><img src="css/images/logo.png" title="系统首页" /></a>
    </div>
        
    <ul class="nav">
    <li><a href="User_doLogin.action" target="rightFrame" class="selected"><img src="css/images/icon03.png" title="工作台" /><h2>工作台</h2></a></li>
    </ul>
            
    <div class="topright">    
    <ul>
    <li><a href="User_doLogout.action" target="_parent" onclick="return confirm('确认退出？')">退出</a></li>
    </ul>
     
    <div class="user">
    <span>${user.userName}</span>
    </div>    
    
    </div>

</body>
</html>
