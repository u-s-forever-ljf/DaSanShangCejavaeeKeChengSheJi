<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>
<%@taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'test.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->

</head>
<!-- 权限标签 -->
<!-- ifAllGranted，例子:只有当前用户同时拥有 ROLE_TEACHER 和 ROLE_USER 两个权限时，才能显示标签内部内容 -->
<!-- ifAnyGranted，例子:如果当前用户拥有 ROLE_TEACHER 或 ROLE_USER 其中一个权限时，就能显示标签内部内容 -->
<!-- ifNotGranted，例子:如果当前用户没有 ROLE_TEACHER 时，才能显示标签内部内容 -->
<body>
	<div>
		<sec:authorize ifAnyGranted="ROLE_TEACHER">
			<% out.println("Hello JSP!");%>
			<% out.println("ROLE_TEACHER JSP!");%>
			<a href="Project_addPrompt.action" target="rightFrame">测试发布课程设计</a></br>
			<a href="Task_addPrompt.action" target="rightFrame">测试发布任务</a></br>
			<a href="SelectCourse_listPrompt.action" target="rightFrame">选课情况列表</a></br>
			<a href="SelectCourse_approvallistPrompt.action" target="rightFrame">审核列表</a></br>
			<a href="SelectCourse_findTask?projectId=2"target="rightFrame"> 测试查任务projectId=2返回json</a></br>
			<a href="Log_listPrompt.action" target="rightFrame">系统日志</a> </br>
			<a href="User_toAdd.action" target="rightFrame">测试添加用户</a> </br>
			<a href="User_listPrompt.action" target="rightFrame">用户列表</a> </br>
			<a href="SelectCourse_addPrompt.action"target="rightFrame" >选课</a></br>
			<a href="SelectCourse_searchTaskByProject.action"target="rightFrame">测试组合查询查课程设计下的任务</a></br>
			<a href="Role_listPrompt.action">角色</a></br>
			<a href="Classroom_listPrompt.action">班级</a></br>
		</sec:authorize>
	</div>
	<sec:authorize ifAllGranted="ROLE_ADMIN">
		<dd>
			<div>
				<a href="Log_listPrompt.action" target="rightFrame">系统日志</a> <a
					href="User_toAdd.action" target="rightFrame">测试添加用户</a> <a
					href="User_listPrompt.action" target="rightFrame">用户列表</a> <a></a>
			</div>
		</dd>
	</sec:authorize>
	<div>
		<sec:authorize ifAnyGranted="ROLE_STUDENT">
			<% out.println("Hello JSP!");%>
			<% out.println("ROLE_STUDENT JSP!");%>
			<a href="SelectCourse_addPrompt.action"target="rightFrame" >选课</a>
		</sec:authorize>
	</div>
</body>
</html>
