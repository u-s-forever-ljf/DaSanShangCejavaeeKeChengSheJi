package org.ssh2.util;


import java.text.SimpleDateFormat;
import java.util.Date;




/**
 * 日期工具类
 * @林景锋
 * 2017年11月19日
 * 
 */
public class DateUtils {

	public static String getCurrentDate() {
		SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return sd.format(new Date());
	}
	
}
