package org.ssh2.web;

/**
 * 配置基础基础控制器接口
 * @林景锋
 * 2017年11月19日
 * 
 *
 */
public interface IBaseController {

	
	String listPrompt();	// 显示列表
	String addPrompt();		// 跳转到增加页面
	String doSave();		// 执行保存操作
	String doBatchDelete();	// 执行批量删除操作
	String updatePrompt();	// 跳转到更新页面
	String doUpdate();		// 执行更新操作
}
