package org.ssh2.web;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.interceptor.RequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.interceptor.SessionAware;

import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionSupport;



public abstract class AbstractBaseController extends ActionSupport implements SessionAware,RequestAware, ServletResponseAware {




	private static final long serialVersionUID = 693945906735785727L;
	
	protected Map<String, Object> request;
	protected HttpServletResponse response;
	protected Map<String, Object> session;
	protected String prompt;
	protected Gson gson = new Gson();
	
	protected Integer pageNo;	// 页码
		
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	@Override
	public void setRequest(Map<String, Object> request) {
		this.request = request;
	}

	@Override
	public void setSession(Map<String, Object> session) {
		this.session = session;		
	}

	public String getPrompt() {
		return prompt;
	}

	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}

	public Gson getGson() {
		return gson;
	}

	public void setGson(Gson gson) {
		this.gson = gson;
	}
	
	@Override
	public void setServletResponse(HttpServletResponse response) {
	   this.response = response;
	}
	
}
