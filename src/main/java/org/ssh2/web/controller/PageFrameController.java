package org.ssh2.web.controller;

import org.ssh2.web.AbstractBaseController;


/**
 * 页面框架控制器
 * @author 林景锋  
 * @date 2017年12月4日
 */
public class PageFrameController extends AbstractBaseController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3321798224999167830L;

	/**
	 * 页面布局
	 * @return
	 */
	public String main(){
		System.out.println("执行main");
		setPrompt("WEB-INF/pages/main.jsp");
		return SUCCESS;
	}
	
	/**
	 * 顶部标题栏
	 * @return
	 */
	public String top(){
		System.out.println("执行top");
		setPrompt("WEB-INF/pages/top.jsp");
		return SUCCESS;
	}
	
	/**
	 * 左边菜单栏
	 * @return
	 */
	public String left(){
		System.out.println("执行left");
		setPrompt("WEB-INF/pages/left.jsp");
		return SUCCESS;
		
	}
	public String middle(){
		System.out.println("执行middle");
		setPrompt("WEB-INF/pages/middle.jsp");
		return SUCCESS;
		
	}

	
	

	/**
	 * 欢迎页
	 * @return
	 */
	public String index(){
		System.out.println("执行index");
		setPrompt("WEB-INF/pages/index.jsp");
		return SUCCESS;
	}


}
