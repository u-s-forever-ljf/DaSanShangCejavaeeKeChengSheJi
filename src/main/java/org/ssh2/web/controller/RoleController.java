package org.ssh2.web.controller;

import java.util.List;

import javax.annotation.Resource;

import org.ssh2.annotation.LogMsg;
import org.ssh2.core.entity.SoftRole;
import org.ssh2.core.entity.SoftUser;
import org.ssh2.core.service.IRoleService;

import org.ssh2.core.util.Page;
import org.ssh2.util.DateUtils;
import org.ssh2.web.AbstractBaseController;
import org.ssh2.web.IBaseController;

public class RoleController extends AbstractBaseController implements IBaseController {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1242465004830962871L;

	private List<Integer> ids;	//	获取复选框的值
	
	
	private SoftRole role;
	@Resource(name = "roleService")
	private IRoleService roleService;
	
	
	/**
	 * 角色列表
	 * @return
	 */
	@Override
	public String listPrompt() {
		Page<SoftRole> page = new Page<SoftRole>();//创建分页对象指定泛型	
		page.setOnePageNum(10);// 设置单页记录数
		page.setPageNo(pageNo == null?1:pageNo);
		page.setClazz("Role");
		roleService.findForPage(page);
		request.put("page", page);
		setPrompt("/WEB-INF/pages/role/role_list.jsp");
		return SUCCESS;
	}
	
	@Override
	public String addPrompt() {
		setPrompt("/WEB-INF/pages/role/role_save.jsp");
		return SUCCESS;
	}

	/*
	 * 新增角色
	 * (non-Javadoc)
	 * @see org.ssh2.web.IBaseController#doSave()
	 */
	@Override
	public String doSave() {
		SoftUser user = (SoftUser) session.get("user");
		role.setCreateDate(DateUtils.getCurrentDate());
		role.setCreateUserName(user.getUsername());
		roleService.save(role);
		setPrompt("Role_listPrompt.action");
		return "reposition";
	}
	/*
	 * 批量删除
	 * (non-Javadoc)
	 * @see org.ssh2.web.IBaseController#doBatchDelete()
	 */

	@Override
	@LogMsg(msg = "删除角色")
	public String doBatchDelete(){
		roleService.batchDelete(ids);
		setPrompt("Role_listPrompt.action");
		return "reposition";
	}
	
	
	/*
	 * 
	 * 把角色id传回后台然后通过id查出角色信息
	 * 然跳转的到页面修改
	 * (non-Javadoc)
	 * @see org.ssh2.web.IBaseController#updatePrompt()
	 */
	@Override
	public String updatePrompt() {
		Integer id = role.getId();
		SoftRole role = roleService.load(id);
		session.put("updateRole", role);
		setPrompt("/WEB-INF/pages/role/role_update.jsp");
		return SUCCESS;
	}

	@Override
	public String doUpdate() {
		
		SoftUser user = (SoftUser) session.get("user");//获取当前session中的用户
		role.setCreateUserName(user.getUsername());
		role.setCreateDate(DateUtils.getCurrentDate());
		roleService.update(role);
		setPrompt("Role_listPrompt.action");
		return "reposition";
	}

	public List<Integer> getIds() {
		return ids;
	}

	public void setIds(List<Integer> ids) {
		this.ids = ids;
	}

	public SoftRole getRole() {
		return role;
	}

	public void setRole(SoftRole role) {
		this.role = role;
	}

	public IRoleService getRoleService() {
		return roleService;
	}

	public void setRoleService(IRoleService roleService) {
		this.roleService = roleService;
	}

}
