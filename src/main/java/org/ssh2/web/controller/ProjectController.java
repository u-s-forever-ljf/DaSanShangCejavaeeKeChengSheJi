package org.ssh2.web.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.ssh2.annotation.LogMsg;
import org.ssh2.core.entity.SoftProject;
import org.ssh2.core.entity.SoftSelectCourse;
import org.ssh2.core.service.IClassroomService;
import org.ssh2.core.service.IProjectService;
import org.ssh2.core.service.IRoleService;
import org.ssh2.core.service.ISelectCourseService;
import org.ssh2.core.service.ITaskService;
import org.ssh2.core.service.IUserService;

import org.ssh2.util.DateUtils;
import org.ssh2.web.AbstractBaseController;
import org.ssh2.web.IBaseController;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
/*
 * 课程设计控制器
 * 林景锋
 * 2017年12月10日
 */
public class ProjectController extends AbstractBaseController implements IBaseController{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4806768821255890048L;

	private List<Integer> ids;	//	获取复选框的值
	
	
	@Resource
	private ISelectCourseService selectCourseService;
	private JSONObject jb;
	public JSONObject getJb() {
		return jb;
	}


	public void setJb(JSONObject jb) {
		this.jb = jb;
	}

	public SoftProject project;
	@Resource
	private IUserService userService;
	
	@Resource
	private IRoleService roleService;
	
	@Resource
	private IProjectService projectService;

	
	@Resource
	private IClassroomService classroomService;
	
	@Resource
	private ITaskService taskService;
	private String projectName;
	
	
	public String getProjectName() {
		return projectName;
	}


	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}


	
	
	
	@Override
	public String listPrompt() {
		// TODO Auto-generated method stub
		return null;
	}

	@LogMsg(msg="查询课程设计全部")
	public String listProject()throws Exception{
		setPrompt("/WEB-INF/pages/project/project_strList.jsp");
		return SUCCESS;
	}
	
	@LogMsg(msg = "模糊查询课程设计")
	public String strlistProject() throws Exception{
		System.out.println("执行了strlistProject()");
		SoftProject pro=new SoftProject();
		jb=new JSONObject();
		JSONArray jr=new JSONArray();
		
		List<SoftProject> projects=projectService.FindByProjectName("projctName", projectName);
		for(SoftProject so:projects){
			String date=so.getEndDate();
			
			List<SoftSelectCourse> status=selectCourseService.find(so.getProjectName());
			String sta="";
			for(SoftSelectCourse slc:status){
				sta=slc.getStatus();
				if(null==sta||"".equals(sta)){
					sta="未审批";
				}
			}
			JSONObject temp=new JSONObject();
			//temp.put("projectid",so.getId());

			temp.put("projectName",so.getProjectName());
			temp.put("teacher",so.getProjectByteacher());
			temp.put("projectcreatetime",so.getCreateDate());
			if(null!=date||!"".equals(date)){
				
				temp.put("projectendtime",so.getEndDate());
			}
			temp.put("projectdesc",so.getProjectDesc());
			temp.put("projectstatus", sta);
		    jr.add(temp);
		}
		System.out.println("执行了strlist()");
		jb.put("rows", jr);
		//setPrompt("/project_strList.jsp");

		return "json";
	}
	

	@LogMsg(msg = "发布课程设计addPrompt")
	public String addPrompt() {
		System.out.println("后台打印一句话");
		
		setPrompt("/WEB-INF/pages/project/project_add.jsp");
		return SUCCESS;
	}

	@LogMsg(msg = "发布课程设计")
	public String addProject(){
		System.out.println("执行了addProject()");
		project.setCreateDate(DateUtils.getCurrentDate());
		projectService.save(project);
		setPrompt("/WEB-INF/pages/main.jsp");
		
		
		return SUCCESS;
		
	}
	@Override
	public String doSave() {
		// TODO Auto-generated method stub

		return null;
	}

	@Override
	public String doBatchDelete() {
		// TODO Auto-generated method stub

		return null;
	}

	@Override
	public String updatePrompt() {
		// TODO Auto-generated method stub

		return null;
	}

	@Override
	public String doUpdate() {
		// TODO Auto-generated method stub

		return null;
	}

	public List<Integer> getIds() {
		return ids;
	}

	public void setIds(List<Integer> ids) {
		this.ids = ids;
	}

	public SoftProject getProject() {
		return project;
	}

	public void setProject(SoftProject project) {
		this.project = project;
	}

	public IUserService getUserService() {
		return userService;
	}

	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

	public IRoleService getRoleService() {
		return roleService;
	}

	public void setRoleService(IRoleService roleService) {
		this.roleService = roleService;
	}

	public IProjectService getProjectService() {
		return projectService;
	}

	public void setProjectService(IProjectService projectService) {
		this.projectService = projectService;
	}

	public IClassroomService getClassroomService() {
		return classroomService;
	}

	public void setClassroomService(IClassroomService classroomService) {
		this.classroomService = classroomService;
	}

	public ITaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(ITaskService taskService) {
		this.taskService = taskService;
	}
 
}