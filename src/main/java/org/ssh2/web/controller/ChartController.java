package org.ssh2.web.controller;





import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;

import org.ssh2.common.Const;
import org.ssh2.core.entity.SoftProject;
import org.ssh2.core.entity.SoftSelectCourse;
import org.ssh2.core.service.IProjectService;
import org.ssh2.core.service.ISelectCourseService;
import org.ssh2.core.service.ITaskService;
import org.ssh2.core.service.IUserService;
import org.ssh2.web.AbstractBaseController;

import com.alibaba.fastjson.JSONObject;
import com.github.abel533.echarts.Option;
import com.github.abel533.echarts.axis.CategoryAxis;
import com.github.abel533.echarts.axis.ValueAxis;
import com.github.abel533.echarts.code.Magic;
import com.github.abel533.echarts.code.MarkType;
import com.github.abel533.echarts.code.Tool;
import com.github.abel533.echarts.code.Trigger;
import com.github.abel533.echarts.data.PointData;
import com.github.abel533.echarts.feature.MagicType;
import com.github.abel533.echarts.series.Bar;
import com.google.gson.Gson;

import net.sf.json.JSONArray;



/**
 * 图表控制器
 * 林景锋
 * 2017年12月10日
 */

@SuppressWarnings("unused")
public class ChartController extends AbstractBaseController{

	/**
	 * 
	 */
	private static final long serialVersionUID = 165092635874017917L;


	public final static String CHART_TITLE = "统计选课审批人数";
	public final static String CHART_SUB_TITLE = "纯属虚构";
	public final static String MAX_VALUE = "最大值";
	public final static String MIN_VALUE = "最小值";
	public final static String AVERAGE_VALUE = "平均值";

	private String result; // 获取图表的JSON数据

	@Resource(name = "userService")
	private IUserService userService;

	@Resource(name = "selectCourseService")
	private ISelectCourseService selectCourseService;

	@Resource(name = "taskService")
	private ITaskService taskService;

	@Resource(name = "projectService")
	private IProjectService projectService;
	// 地址：http://echarts.baidu.com/doc/example/bar1.html
	public String createChart() {
		response.setContentType("textml; charset=UTF-8");
		System.out.println("createChart");
		Option option = new Option();	
		option.title().text(CHART_TITLE).subtext(CHART_SUB_TITLE);
		option.tooltip().trigger(Trigger.axis);
		//System.out.println("option.tooltip().trigger(Trigger.axis);"+option);
		option.legend(Const.APPROVAL,Const.DISAPPROVAL);//审批和未审批
		//System.out.println("option.legend(Const.APPROVAL,Const.DISAPPROVAL)"+option);
		option.toolbox()
		.show(true)
		.feature(Tool.mark, Tool.dataView,
				new MagicType(Magic.line, Magic.bar).show(true),
				Tool.restore, Tool.saveAsImage);
		option.calculable(true);

		//System.out.println("option.toolbox()"+option);
		Map<String, Integer> notDetectMap = getNotDetectApply();
		Map<String, Integer> detectMap = getDetectApply();

		String[] projectNames = getAllProjectName();
		//System.out.println("projectNames="+projectNames);
		//设置横坐标
		
		option.xAxis(new CategoryAxis().data(projectNames));

		// 设置纵坐标
		option.yAxis(new CategoryAxis());
		List<Integer> detectValue = new ArrayList<Integer>();
		Bar bar = new Bar(Const.APPROVAL);
		for(int i=0;i<projectNames.length;i++){
			// 遍历map中的值获取已审批表add中
			for(Entry<String, Integer> m : detectMap.entrySet()){
				if(m.getKey().equals(projectNames[i])){
					detectValue.add(m.getValue());
				}
			}
		}

		bar.data(detectValue.toArray());
		bar.markPoint().data(
				new PointData().type(MarkType.max).name(MAX_VALUE),
				new PointData().type(MarkType.min).name(MIN_VALUE));
		bar.markLine().data(
				new PointData().type(MarkType.average).name(AVERAGE_VALUE));
		Bar bar2 = new Bar(Const.DISAPPROVAL);

		List<Integer> notDetectValue = new ArrayList<Integer>();
		for (int i = 0; i < projectNames.length; i++) {
			// 遍历map中的值
			for (Entry<String, Integer> m : notDetectMap.entrySet()) {
				if (m.getKey().equals(projectNames[i])){
					notDetectValue.add(m.getValue());
				}
			}
		}

		bar2.data(notDetectValue.toArray());
		bar2.markPoint().data(
				new PointData().type(MarkType.max).name(MAX_VALUE),
				new PointData().type(MarkType.min).name(MIN_VALUE));
		bar2.markLine().data(
				new PointData().type(MarkType.average).name(AVERAGE_VALUE));
		option.series(bar, bar2);

		Gson gson = new Gson();
		result = gson.toJson(option);

		return SUCCESS;

	}

	/**
	 * 获取已审批表
	 * 
	 * @date 2017年12月23日
	 */
	public Map<String,Integer> getDetectApply(){
		return getApplyByStatus(Const.APPROVAL);
	}

	/**
	 * 获取未审批表
	 * 
	 * @date 2017年12月23日
	 */
	public Map<String, Integer> getNotDetectApply() {
		return getApplyByStatus(Const.DISAPPROVAL);
	}


	/**
	 * 根据状态获取申请表
	 * 
	 * @param status
	 * @return
	 */
	public Map<String, Integer> getApplyByStatus(String status) {
		Map<String, Integer> map = new HashMap<String, Integer>();
//		final String hql = "from SoftSelectCourse where status = ?";
		response.setContentType("textml; charset=UTF-8");
//		JSONObject json = new JSONObject();
//		JSONArray result = new JSONArray();
		// 查询未检测的申请表
		List<SoftSelectCourse> applyList = selectCourseService.liststatus(status);
		System.out.println("applyList=getApplyByStatus="+applyList);
//		for(SoftSelectCourse a:applyList){
//			JSONObject obj = new JSONObject();
//			obj.put("createDate", a.getCreateDate());
//			obj.put("dealDate", a.getDealDate());
//			obj.put("projectName", a.getProjectName());
//			obj.put("taskName", a.getTaskName());
//			obj.put("selectName", a.getSelectName());
//			obj.put("desc", a.getDesc());
//			obj.put("approver", a.getApprover());
//			obj.put("status", a.getStatus());
//			result.add(obj);
//			System.out.println("1="+a.getStatus()+"2="+a.getProjectName());
//		}
//		json.put("result", result);
//		System.out.println(result);
		for(int i = 0;i < applyList.size(); i++){
			String projectName = applyList.get(i).getProjectName();
			if(map.containsKey(projectName)){
				int val = map.get(projectName) + 1; // 如果已经包含则数量+1
				map.put(projectName, val);// 将+1后的值存放回map
			}else{
				// 如果未包含则初始化为1
				map.put(projectName, 1);
			}
		} 
		System.out.println(map);
		return map;
	}

	/**
	 * 把所有的课程设计数据转成字符数组
	 * 
	 * @return
	 */

	public String[] getAllProjectName(){

		List<SoftProject> applyList = projectService.getAllFromTable();
		System.out.println("applyListgetAllLabName"+applyList);
		response.setContentType("textml; charset=UTF-8");
//		JSONObject json = new JSONObject();
//		JSONArray result = new JSONArray();
//		for(SoftProject a:applyList){
//			JSONObject obj = new JSONObject();
//			obj.put("projectName", a.getProjectName());
//			obj.put("projectByteacher", a.getProjectByteacher());
//			obj.put("createDate", a.getCreateDate());
//			obj.put("endDate", a.getEndDate());
//			obj.put("projectDesc", a.getProjectDesc());
//			result.add(obj);
//			System.out.println("11=="+a.getProjectName());
//		}
//		json.put("result", result);....
		List<String> list = new ArrayList<String>();
		for (int i =0 ; i < applyList.size();i++){
			list.add(applyList.get(i).getProjectName());
		}
		String[] strings = new String[list.size()];
		return list.toArray(strings);


	}


	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}


}
