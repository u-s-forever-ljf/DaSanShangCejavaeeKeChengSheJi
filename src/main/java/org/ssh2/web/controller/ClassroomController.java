package org.ssh2.web.controller;

import java.util.List;

import javax.annotation.Resource;

import org.ssh2.core.entity.SoftClassroom;
import org.ssh2.core.entity.SoftUser;
import org.ssh2.core.service.IClassroomService;
import org.ssh2.core.util.Page;
import org.ssh2.util.DateUtils;
import org.ssh2.web.AbstractBaseController;
import org.ssh2.web.IBaseController;

public class ClassroomController extends AbstractBaseController implements IBaseController{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5537975702958588778L;

	private List<Integer> ids;	//	获取复选框的值

	private SoftClassroom classroom;
	@Resource(name = "classroomService")
	private IClassroomService classroomService;


	@Override
	public String listPrompt() {
		Page<SoftClassroom> page = new Page<SoftClassroom>();
		page.setOnePageNum(10);
		page.setPageNo(pageNo==null?1:pageNo);
		page.setClazz("Classroom");
		classroomService.findForPage(page);
		request.put("page", page);
		setPrompt("/WEB-INF/pages/classroom/classroom_list.jsp");
		return SUCCESS;
	}

	@Override
	public String addPrompt() {
		setPrompt("/WEB-INF/pages/classroom/classroom_save.jsp");
		return SUCCESS;
	}

	@Override
	public String doSave() {
		SoftUser user = (SoftUser) session.get("user");
		classroom.setCreateUserName(user.getUserName());
		classroom.setCreateDate(DateUtils.getCurrentDate());
		classroomService.save(classroom);
		setPrompt("Classroom_listPrompt.action");
		return "reposition";
	}

	@Override
	public String doBatchDelete() {
		classroomService.batchDelete(ids);
		setPrompt("Classroom_listPrompt.action");
		return "reposition";
	}

	@Override
	public String updatePrompt() {
		Integer id = classroom.getId();
		SoftClassroom classroom = classroomService.load(id);
		session.put("updateClassroom", classroom);
		setPrompt("/WEB-INF/pages/classroom/classroom_update.jsp");
		return SUCCESS;
	}

	@Override
	public String doUpdate() {
		SoftUser user = (SoftUser) session.get("user");
		classroom.setCreateUserName(user.getUsername());
		classroom.setCreateDate(DateUtils.getCurrentDate());
		classroomService.update(classroom);
		setPrompt("Classroom_listPrompt.action");
		return "reposition";
	}

	public List<Integer> getIds() {
		return ids;
	}

	public void setIds(List<Integer> ids) {
		this.ids = ids;
	}

	public SoftClassroom getClassroom() {
		return classroom;
	}

	public void setClassroom(SoftClassroom classroom) {
		this.classroom = classroom;
	}

	public IClassroomService getClassroomService() {
		return classroomService;
	}

	public void setClassroomService(IClassroomService classroomService) {
		this.classroomService = classroomService;
	}

}
