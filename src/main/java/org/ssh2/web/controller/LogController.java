package org.ssh2.web.controller;



import javax.annotation.Resource;

import org.ssh2.annotation.LogMsg;
import org.ssh2.core.entity.SoftLog;
import org.ssh2.core.service.ILogService;
import org.ssh2.core.util.Page;
import org.ssh2.web.AbstractBaseController;
/**
 * 日志控制器
 * @林景锋
 * 2017年11月27日
 *
 */
public class LogController extends AbstractBaseController {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7586360895369326179L;

	@Resource(name = "logService")
	private ILogService logService;
	
	
	public String listPrompt(){
		Page<SoftLog> page = new Page<SoftLog>();//创建分页对象指定泛型	
		page.setOnePageNum(10);//设置单页记录数
		page.setPageNo(pageNo==null?1:pageNo);//设置当前页数 默认1
		page.setClazz("Log");
		logService.findForPage(page);
		request.put("page", page);
		setPrompt("/WEB-INF/pages/log/log_list.jsp");
		return SUCCESS;
	}
	
	
}
