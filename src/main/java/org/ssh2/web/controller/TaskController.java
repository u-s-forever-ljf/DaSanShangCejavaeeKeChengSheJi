package org.ssh2.web.controller;

import java.util.List;

import javax.annotation.Resource;

import org.ssh2.annotation.LogMsg;
import org.ssh2.core.entity.SoftProject;
import org.ssh2.core.entity.SoftTask;
import org.ssh2.core.service.IClassroomService;
import org.ssh2.core.service.IProjectService;
import org.ssh2.core.service.IRoleService;
import org.ssh2.core.service.ITaskService;
import org.ssh2.core.service.IUserService;
import org.ssh2.util.DateUtils;
import org.ssh2.web.AbstractBaseController;
import org.ssh2.web.IBaseController;

public class TaskController extends AbstractBaseController implements IBaseController {

	/**
	 * 
	 */
	private static final long serialVersionUID = -80726151789320431L;
	private List<Integer> ids;	//	获取复选框的值
	
	public SoftTask task;
	@Resource
	private IUserService userService;
	
	@Resource
	private IRoleService roleService;
	
	@Resource
	private IProjectService projectService;

	
	@Resource
	private IClassroomService classroomService;
	
	@Resource
	private ITaskService taskService;
	
	
	/**
	 * 林景锋
	 * 获取课程设计列表
	 * 2017年12月9日
	 */
	public List<SoftProject> getAllProject(){
		return projectService.getAllFromTable();
		
	}
	
	@Override
	public String listPrompt() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@LogMsg(msg = "发布任务addPrompt")
	public String addPrompt() {
		
		session.put("projects", getAllProject());// 将课程设计数据传到前台
		System.out.println("projectList"+getAllProject());// 将课程设计数据传到前台
		System.out.println("后台打印一句话addPrompt");
		setPrompt("/WEB-INF/pages/task/task_add.jsp");
		return SUCCESS;
	}



	public String addTask(){
		System.out.println("执行了addProject()");

		task.setCreateDate(DateUtils.getCurrentDate());
//		task.setProject();
		taskService.save(task);

		setPrompt("/WEB-INF/pages/main.jsp");
		return SUCCESS;
	}
	
	@Override
	public String doSave() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String doBatchDelete() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String updatePrompt() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String doUpdate() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Integer> getIds() {
		return ids;
	}

	public void setIds(List<Integer> ids) {
		this.ids = ids;
	}

	public SoftTask getTask() {
		return task;
	}

	public void setTask(SoftTask task) {
		this.task = task;
	}

	public IUserService getUserService() {
		return userService;
	}

	public void setUserService(IUserService userService) {
		this.userService = userService;
	}

	public IRoleService getRoleService() {
		return roleService;
	}

	public void setRoleService(IRoleService roleService) {
		this.roleService = roleService;
	}

	public IProjectService getProjectService() {
		return projectService;
	}

	public void setProjectService(IProjectService projectService) {
		this.projectService = projectService;
	}

	public IClassroomService getClassroomService() {
		return classroomService;
	}

	public void setClassroomService(IClassroomService classroomService) {
		this.classroomService = classroomService;
	}

	public ITaskService getTaskService() {
		return taskService;
	}

	public void setTaskService(ITaskService taskService) {
		this.taskService = taskService;
	}

}
