package org.ssh2.web.controller;

import java.io.IOException;

import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.ssh2.annotation.LogMsg;

import org.ssh2.core.entity.SoftProject;
import org.ssh2.core.entity.SoftSelectCourse;
import org.ssh2.core.entity.SoftTask;
import org.ssh2.core.entity.SoftUser;

import org.ssh2.core.service.IProjectService;
import org.ssh2.core.service.ISelectCourseService;
import org.ssh2.core.service.ITaskService;
import org.ssh2.core.util.Page;
import org.ssh2.util.DateUtils;
import org.ssh2.web.AbstractBaseController;
import org.ssh2.web.IBaseController;

import com.alibaba.fastjson.JSONObject;

import net.sf.json.JSONArray;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;



/**
 * 
 * 选课控制器
 * 林景锋
 * 2017年12月10
 * 
 */
public class SelectCourseController extends AbstractBaseController implements IBaseController{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5289686162269337122L;

	private List<Integer> ids;	//	获取复选框的值
	private String result; // 返回到客户端的数据


	public SoftSelectCourse selectCourse;

	public int ssid;

	private Integer projectId;


	
	private Integer taskprojectId;

	HttpServletResponse response =ServletActionContext.getResponse();// 获取response



	@Resource(name = "projectService")
	private IProjectService projectService;
	@Resource(name = "taskService")
	private ITaskService taskService;
	@Resource(name = "selectCourseService")
	private ISelectCourseService selectCourseService;


	/**
	 * 获取课程设计
	 * 
	 * @return
	 */
	public String findProject(){

		/*
		 * 把一个存放多个实体list放入json中，
		 * 因为存入的实体用hibernate与其它 实体之间的关联关系，
		 * 所以会报一个死循环的错。
		 * 用你这种方式，config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		 * 确实不会报错，但其实在客户端就取不到过滤的值了。
		 */
		List<SoftProject> projects = projectService.getAllFromTable();
		JsonConfig config = new JsonConfig();
		config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);// 防止因自包含出现死循环的问题
		config.setExcludes(new String[] { "tasks" });// 过滤掉任务
		JSONArray jArray = JSONArray.fromObject(projects, config);
		result = jArray.toString();
		System.out.println("findProject"+result);
		return SUCCESS;

	}
	/**
	 * 获取课程设计所包含的任务
	 * 
	 * @return
	 * @throws IOException 
	 */

		public String findTask(){
		System.out.println("findTask");
		System.out.println(projectId);
		response.setContentType("textml; charset=UTF-8");
		JSONObject json = new JSONObject();
		JSONArray result = new JSONArray();
		List<SoftTask> tasks = taskService.findTaskByProjectId(projectId);

		for(SoftTask a:tasks){
			JSONObject obj = new JSONObject();
			obj.put("taskId",a.getId() );
			obj.put("taskName", a.getTaskName());
			obj.put("createDate", a.getCreateDate());
			obj.put("endDate", a.getEndDate());
			obj.put("taskByteacher", a.getTaskByteacher());
			obj.put("taskDesc", a.getTaskDesc());
			result.add(obj);
			System.out.println("1="+a.getId()+"2="+a.getTaskName());
		}
		json.put("result", result);
		System.out.println("运行一句话qian"+result);
		response.setContentType("text/json");
		try {
			response.getWriter().write(json.toJSONString());
			System.out.println(json);
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("运行一句话");
		return null;
}
		
		



	/**
	 * 选课申请列表
	 */
	@Override
	@LogMsg(msg = "选课listPrompt")
	public String listPrompt() {

		Page<SoftSelectCourse> page = new Page<SoftSelectCourse>(); // 创建分页对象指定泛型
		page.setOnePageNum(10);// 设置单页记录数
		page.setPageNo(pageNo == null?1:pageNo);// 设置当前页数 默认1
		page.setClazz("SelectCourse");
		selectCourseService.findForPage(page);
		request.put("page", page);
		setPrompt("/WEB-INF/pages/selectcourse/select_course_list.jsp");
		return SUCCESS;
	}

	//跳转页面
	@Override
	public String addPrompt() {
		setPrompt("/WEB-INF/pages/selectcourse/select_course_student.jsp");
		return SUCCESS;
	}


	@Override
	@LogMsg(msg = "选课申请")
	public String doSave() {
		SoftUser user = (SoftUser) session.get("user");
		selectCourse.setSelectName(user.getUserName());
		System.out.println("projectId="+projectId);
		System.out.println("taskprojectId="+taskprojectId);
		SoftProject project = projectService.load(projectId);
		SoftTask task = taskService.load(taskprojectId);
		selectCourse.setCreateDate(DateUtils.getCurrentDate());
		selectCourse.setProjectName(project.getProjectName());
		selectCourse.setTaskName(task.getTaskName());
		selectCourseService.save(selectCourse);
		setPrompt("SelectCourse_listPrompt.action");

		return "reposition";
	}

	/**
	 * 选课审核列表
	 */
	@LogMsg(msg = "选课审核页面跳转")
	public String approvallistPrompt(){
		Page<SoftSelectCourse> page = new Page<SoftSelectCourse>(); // 创建分页对象指定泛型
		page.setOnePageNum(10);// 设置单页记录数
		page.setPageNo(pageNo == null ? 1 : pageNo);// 设置当前页数 默认1
		page.setClazz("SelectCourse");
		selectCourseService.findForPage(page);
		request.put("page", page);
		setPrompt("/WEB-INF/pages/selectcourse/select_course.jsp");
		return SUCCESS;
	}
	@Override
	@LogMsg(msg="批量删除doBatchDelete")
	public String doBatchDelete() {

		selectCourseService.batchDelete(ids);
		setPrompt("SelectCourse_listPrompt.action");
		return "reposition";
	}

	@Override
	@LogMsg(msg="跳转更新页面给分数")
	public String updatePrompt() {

		System.out.println("跳转更新页面给分数ssid="+ssid);
		SoftSelectCourse selectCourse = selectCourseService.load(ssid);
		session.put("updateSelectCourse", selectCourse);
		System.out.println("SoftSelectCourse apply = selectCourseService.load(ssid);"+selectCourse);
		setPrompt("/WEB-INF/pages/selectcourse/select_course_score.jsp");
		return SUCCESS;
	}
	public String searchTaskByProject(){
		List<SoftProject> projectList = projectService.getAllFromTable();

		request.put("projectList", projectList);
		setPrompt("/WEB-INF/pages/task/searchTaskByProject.jsp");
		
		return SUCCESS;
	}

	public String UpdateById(){

		SoftUser user = (SoftUser) session.get("user");
		SoftSelectCourse se=selectCourseService.loadById(ssid);
		se.setApprover(user.getUserName());
		se.setStatus("已审批");

		se.setDealDate(DateUtils.getCurrentDate());


		selectCourseService.update(se);
		setPrompt("/WEB-INF/pages/main.jsp");
		return SUCCESS;
	}
	public String UpdateById2(){

		SoftUser user = (SoftUser) session.get("user");
		SoftSelectCourse se=selectCourseService.loadById(ssid);
		se.setApprover(user.getUserName());
		se.setStatus("未审批");

		se.setDealDate(DateUtils.getCurrentDate());


		selectCourseService.update(se);
		setPrompt("/WEB-INF/pages/main.jsp");
		return SUCCESS;
	}
	
	//评分更新
	@Override
	public String doUpdate() {
		selectCourseService.update(selectCourse);
		setPrompt("SelectCourse_listPrompt.action");
		return "reposition";


	}
	public List<Integer> getIds() {
		return ids;
	}
	public void setIds(List<Integer> ids) {
		this.ids = ids;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public SoftSelectCourse getSelectCourse() {
		return selectCourse;
	}
	public void setSelectCourse(SoftSelectCourse selectCourse) {
		this.selectCourse = selectCourse;
	}
	public int getSsid() {
		return ssid;
	}
	public void setSsid(int ssid) {
		this.ssid = ssid;
	}
	public Integer getProjectId() {
		return projectId;
	}
	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	public HttpServletResponse getResponse() {
		return response;
	}
	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}
	public IProjectService getProjectService() {
		return projectService;
	}
	public void setProjectService(IProjectService projectService) {
		this.projectService = projectService;
	}
	public ITaskService getTaskService() {
		return taskService;
	}
	public void setTaskService(ITaskService taskService) {
		this.taskService = taskService;
	}
	public ISelectCourseService getSelectCourseService() {
		return selectCourseService;
	}
	public void setSelectCourseService(ISelectCourseService selectCourseService) {
		this.selectCourseService = selectCourseService;
	}
	public Integer getTaskprojectId() {
		return taskprojectId;
	}
	public void setTaskprojectId(Integer taskprojectId) {
		this.taskprojectId = taskprojectId;
	}






	//
	//	/**
	//	 * 弹窗
	//	 * 
	//	 * @return
	//	 */
	//	public void bombWindow(String msg, String href) {
	//		super.response.setContentType("text/html;charset=UTF-8");
	//		super.response.setCharacterEncoding("UTF-8");// 防止弹出的信息出现乱码
	//		try {
	//			PrintWriter out = response.getWriter();
	//			out.print("<script>alert('" + msg + "')</script>");
	//			out.print("<script>window.location.href='" + href + "'</script>");
	//			out.flush();
	//			out.close();
	//		} catch (IOException e) {
	//			e.printStackTrace();
	//		}
	//	}

	
	/*
	 * 
	 * HttpServletResponse response =ServletActionContext.getResponse();// 获取response
		response.setContentType("textml; charset=UTF-8");
		JSONObject json = new JSONObject();
		JSONArray result = new JSONArray();
		for(OaEmployeeRight c:oaEmployeeRightList){
			JSONObject obj = new JSONObject();
			obj.put("id", c.getId());
			obj.put("name", c.getEmployeeName());
			result.add(obj);
		}
		json.put("result", result);
		json.put("rightType", oaEmployeeRight.getRightType());
		System.out.println(json);
		response.setContentType("text/json");
		try {
			response.getWriter().write(json.toJSONString());
		} catch (IOException e) {
			e.printStackTrace();
		}

//		 */
	//		PrintWriter pw = response.getWriter();
	//		System.out.println("PrintWriter pw = response.getWriter();");
	//		try {
	//			
	//			Gson g= new Gson();
	//			System.out.println("Gson g= new Gson();="+g);
	//			System.out.println("asd"+tasks.size());
	//			String json =g.toJson(tasks);
	//			System.out.println("json="+json);
	//			pw.print(json);
	//			System.out.println("zxc");
	//			pw.flush();
	//		
	//			
	//		}finally{
	//			pw.close();
	//		}
	
	/*	public String findTask(){

	List<SoftTask> tasks = taskService.findTaskByProjectId(projectId);
	for(SoftTask a:tasks){
		
		System.out.println("1="+a.getId()+"2="+a.getTaskName());
	}
	System.out.println("List<SoftTask> tasks"+tasks);
	System.out.println(tasks.toString());
	JsonConfig config = new JsonConfig();
	config.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);// 防止因自包含出现死循环的问题
	config.setExcludes(new String[] { "projects" });// 过滤掉课程设计
	JSONArray jArray =JSONArray.fromObject(tasks, config);
	result = jArray.toString();
	System.out.println("findTask"+result);
	return SUCCESS;
}*/
}
