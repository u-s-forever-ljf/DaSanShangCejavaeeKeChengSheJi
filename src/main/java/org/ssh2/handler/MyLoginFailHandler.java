package org.ssh2.handler;

import java.io.IOException;



import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.ssh2.common.Const;
/**
 * 配置登陆失败处理器
 * @林景锋
 * 2017年11月23日
 * 这个是关联spring-securtity.xml
 * lv1.1
 */
public class MyLoginFailHandler implements AuthenticationFailureHandler {

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		System.out.println("登录失败");
		System.out.println("登录失败");
		System.out.println("登录失败");System.out.println("登录失败");
		response.sendRedirect("login.jsp");
		
		HttpSession session = request.getSession();
		if(session != null){
			session.setAttribute("msg", Const.LOGIN_ERROE_MSG);
		}

	}

}
