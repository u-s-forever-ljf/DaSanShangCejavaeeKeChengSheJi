package org.ssh2.handler;

import java.io.IOException;

import javax.annotation.Resource;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import org.ssh2.core.entity.SoftUser;
import org.ssh2.core.service.IUserInfoService;


/**
 * 配置登录成功处理器
 * @林景锋
 * 2017年11月23日
 * 这个是关联spring-securtity.xml
 * lv1.1
 */
public class MyLoginSuccessHandler implements AuthenticationSuccessHandler {

	//注入userInfoService
	@Resource(name = "userInfoService")
	private IUserInfoService userInfoService;
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		System.out.println("onAuthenticationSuccess");
		System.out.println("onAuthenticationSuccess");
		System.out.println("onAuthenticationSuccess");

		SoftUser user = null;
		Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if(obj !=null && obj instanceof SoftUser){
			user = (SoftUser) obj;
			System.out.println("hello.html111");
			response.sendRedirect("User_doLogin.action");
			System.out.println("hello.html");
			System.out.println("密码：" + user.getPassword());
			HttpSession session = request.getSession();
			if(session != null){
				session.setAttribute("user", user);
			}
			
		}
	}

}
