package org.ssh2.common;




/**
 * 常量
 * 林景锋
 * 用来做提示信息之类的使用
 *
 */
public class Const {

	// 登陆
	public final static String LOGIN_ERROE_MSG = "用户名或密码错误";	
	public final static String LOGOUT_MSG = "退出登陆成功";
	
	// 审批
	public final static String APPROVAL = "已审批";	
	public final static String DISAPPROVAL = "未审批";	
}
