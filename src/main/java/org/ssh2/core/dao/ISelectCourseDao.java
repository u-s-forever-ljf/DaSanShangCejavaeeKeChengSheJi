package org.ssh2.core.dao;

import java.util.List;

import org.ssh2.core.entity.SoftSelectCourse;

public interface ISelectCourseDao extends IBaseDao<SoftSelectCourse>{

   public SoftSelectCourse loadById(int id);
	
	public  List<SoftSelectCourse> find(String name);

}
