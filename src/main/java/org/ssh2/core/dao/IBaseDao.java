package org.ssh2.core.dao;

import java.util.List;

import org.ssh2.core.util.Page;

/**
 * 公共dao接口
 * @林景锋  
 * @2017年11月19日
 */
public interface IBaseDao<T> {

	void save(T t);  

	void delete(int id);  

	void update(T t);

	T load(int id);  

	T getUniqueResult(String hql, Object... args);
	
	List<T> list(String hql, Object...args);

	List<T> getAllFromTable();	// 获取单张表的所有数据

	boolean findForPage(Page<T> page);	// 分页查询

	boolean findPageByCondition(Page<T> page,String condition);	// 通过条件进行分页查询

	void batchDelete(final List<Integer> ids); // 批量删除

	void saveOrUpdate(T t);
}
