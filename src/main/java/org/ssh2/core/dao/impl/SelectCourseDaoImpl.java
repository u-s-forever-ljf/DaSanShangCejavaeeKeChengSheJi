package org.ssh2.core.dao.impl;




import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.ssh2.core.dao.ISelectCourseDao;
import org.ssh2.core.entity.SoftSelectCourse;

@Repository("selectCourseDao")
public class SelectCourseDaoImpl extends BaseDaoImpl<SoftSelectCourse> implements ISelectCourseDao{

	
	@Resource(name = "sessionFactory")
	public void setSuperSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	@Override
	public SoftSelectCourse loadById(int id) {
		System.out.println("int id"+id);
		String hql = "from SoftSelectCourse where id = ?";
		Query query = (Query) this.getSession().createQuery(hql);
		query.setInteger(0, id);
		SoftSelectCourse se =(SoftSelectCourse)query.uniqueResult();
		System.out.println("public SoftSelectCourse loadById");
		
		return se;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public   List<SoftSelectCourse> find(String name) {
		Query qu=getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery("from SoftSelectCourse where projectName like ?");
		qu.setString(0, "%"+name+"%");
		return qu.list();
	}
	public List<SoftSelectCourse> liststatus(String status) {
		String hql = "from SoftSelectCourse where status = ?";
		Query query = this.getSession().createQuery(hql);
	       query.setParameter(0, status);
	      List<SoftSelectCourse> applyList= query.list();
	      return applyList;
	}


	
	


}
