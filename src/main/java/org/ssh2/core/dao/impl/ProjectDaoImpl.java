package org.ssh2.core.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.ssh2.core.dao.IProjectDao;
import org.ssh2.core.entity.SoftProject;



@Repository("projectDao")
public class ProjectDaoImpl extends BaseDaoImpl<SoftProject> implements IProjectDao{

	@Resource(name = "sessionFactory")
	public void setSuperSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@SuppressWarnings({ "unchecked" })
	@Override
	public List<SoftProject> getAllFromSoftProject() {
		String hql = "from SoftProject";
		Query query = this.getSession().createQuery(hql);
		//设置占位符,你要projectname 没传过来，查全部的话不用这个条件把

		List<SoftProject> list = query.list();
		return list;

	}
	
	@SuppressWarnings({ "unchecked"})
	@Override
	public List<SoftProject> FindByProjectName(String projectName, String value) {
		String hql = "from SoftProject where projectName like ?";
		
		Query query = getHibernateTemplate().getSessionFactory().getCurrentSession().createQuery(hql);
		query.setString(0, "%"+value+"%");
		return query.list();
		/*SoftProject project = (SoftProject) query.uniqueResult();

		System.out.println("projectName"+projectName);

		if (projectName!= null) {

			query = this.getSession().createQuery(

					"from SoftProject where project_name = %'" +projectName+ "'%");

		} if (value!= null) {

			query = this.getSession().createQuery(

					"from SoftProject where value = %'" +value+ "'%");

		}

		return  query.list();*/
	}

}