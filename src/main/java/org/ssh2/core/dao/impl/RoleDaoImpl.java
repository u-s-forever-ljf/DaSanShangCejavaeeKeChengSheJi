package org.ssh2.core.dao.impl;

import org.springframework.stereotype.Repository;
import org.ssh2.core.entity.SoftRole;

@Repository("roleDao")
public class RoleDaoImpl extends BaseDaoImpl<SoftRole> implements org.ssh2.core.dao.IRoleDao{

}
