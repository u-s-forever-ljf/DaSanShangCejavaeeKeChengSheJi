package org.ssh2.core.dao.impl;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.ssh2.core.dao.ITaskDao;
import org.ssh2.core.entity.SoftTask;


@Repository("taskDao")
public class TaskDaoImpl extends BaseDaoImpl<SoftTask> implements ITaskDao {
	@Resource(name = "sessionFactory")
	public void setSuperSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<SoftTask> listbyId(int id) {
	
		String hql = "from SoftTask WHERE project_id = ?";
       Query query = this.getSession().createQuery(hql);
       query.setParameter(0, id);
       List<SoftTask> tasks = query.list();
       return tasks;
       
		
	
	}

}
