package org.ssh2.core.dao.impl;

import org.springframework.stereotype.Repository;
import org.ssh2.core.dao.IClassroomDao;
import org.ssh2.core.entity.SoftClassroom;


@Repository("classroomDao")
public class ClassroomDaoImpl extends BaseDaoImpl<SoftClassroom> implements IClassroomDao {

}
