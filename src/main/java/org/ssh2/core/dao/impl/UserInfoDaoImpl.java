package org.ssh2.core.dao.impl;

import javax.annotation.Resource;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.stereotype.Repository;
import org.ssh2.core.dao.IUserInfoDao;
import org.ssh2.core.entity.SoftUser;




@Repository("userInfoDao")
public class UserInfoDaoImpl extends HibernateDaoSupport implements
		IUserInfoDao {


	@Resource(name = "sessionFactory")
	public void setSuperSessionFactory(SessionFactory sessionFactory) {
		super.setSessionFactory(sessionFactory);
	}

	@Override
	public SoftUser getUserByName(String username) {
//		String password1 =pwd.getMd5(password1);
		Query query = this.getSession().createQuery(
				"from SoftUser where user_name = ?");
		query.setString(0, username);
		SoftUser user = (SoftUser) query.uniqueResult();
		System.out.println("UserInfoDaoImpl"+"  "+user);
		if (user == null) {
			return null;
		}else{
		System.out.println("UserInfoDaoImpl"+"  "+user);
		System.out.println("UserInfoDaoImpl"+"  "+user);
		System.out.println("UserInfoDaoImpl"+"  "+user);
			return user;
		}
	}

}


