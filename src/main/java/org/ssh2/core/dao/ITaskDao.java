package org.ssh2.core.dao;

import java.util.List;

import org.ssh2.core.entity.SoftTask;

public interface ITaskDao extends IBaseDao<SoftTask>{
	public List<SoftTask> listbyId(int id);
}
