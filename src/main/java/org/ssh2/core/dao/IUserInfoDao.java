package org.ssh2.core.dao;

import org.ssh2.core.entity.SoftUser;
//用户信息接口
public interface IUserInfoDao {

	
	public SoftUser getUserByName(String username);
}
