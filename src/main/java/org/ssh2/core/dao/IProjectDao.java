package org.ssh2.core.dao;

import java.util.List;

import org.ssh2.core.entity.SoftProject;

public interface IProjectDao extends IBaseDao<SoftProject> {
	
	List<SoftProject> getAllFromSoftProject();

	 List<SoftProject> FindByProjectName(String projectName,String value);
	
}
