package org.ssh2.core.service;

import java.util.List;

import org.ssh2.core.entity.SoftTask;

public interface ITaskService extends IBaseService<SoftTask>{

	
	List<SoftTask>findTaskByProjectId(int id);
}
