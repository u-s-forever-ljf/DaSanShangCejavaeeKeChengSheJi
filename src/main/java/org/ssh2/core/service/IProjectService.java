package org.ssh2.core.service;

import java.util.List;

import org.ssh2.core.entity.SoftProject;

public interface IProjectService extends IBaseService<SoftProject>{

    List<SoftProject> getAllFromSoftProject();
	
	List<SoftProject> FindByProjectName(String projectName, String value);
}
