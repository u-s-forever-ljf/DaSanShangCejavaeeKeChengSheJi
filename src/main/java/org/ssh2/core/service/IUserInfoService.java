package org.ssh2.core.service;

//import org.springframework.security.core.userdetails.UserDetailsService;

////总之就是要身份验证
//public interface IUserInfoService extends UserDetailsService{
//	/* 
//	 * UserDetailsService在身份认证中的作用
//
//	  Spring Security中进行身份验证的是AuthenticationManager接口，
//	  ProviderManager是它的一个默认实现，但它并不用来处理身份认证，
//	    而是委托给配置好的AuthenticationProvider，
//	    每个AuthenticationProvider会轮流检查身份认证。
//	    检查后或者返回Authentication对象或者抛出异常。
//
//	    验证身份就是加载响应的UserDetails，
//	    看看是否和用户输入的账号、密码、权限等信息匹配。
//	    此步骤由实现AuthenticationProvider的DaoAuthenticationProvider
//	    （它利用UserDetailsService验证用户名、密码和授权）处理。
//	    包含 GrantedAuthority 的 UserDetails对象在构建 Authentication对象时填入数据。
//	 */
//}
import org.springframework.security.core.userdetails.UserDetailsService;

public interface IUserInfoService extends UserDetailsService{

}
