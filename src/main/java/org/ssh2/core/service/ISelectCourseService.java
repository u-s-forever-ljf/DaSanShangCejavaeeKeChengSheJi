package org.ssh2.core.service;


import java.util.List;

import org.ssh2.core.entity.SoftSelectCourse;

public interface ISelectCourseService extends IBaseService<SoftSelectCourse>{

	SoftSelectCourse loadById(int ssid);
	public  List<SoftSelectCourse> find(String name);
	List<SoftSelectCourse> liststatus(String status);

}
