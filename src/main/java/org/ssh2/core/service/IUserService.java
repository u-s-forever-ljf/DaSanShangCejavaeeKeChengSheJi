package org.ssh2.core.service;

import org.ssh2.core.entity.SoftUser;

public interface IUserService extends IBaseService<SoftUser>{

	SoftUser findByUserName(String userName);

	boolean findByUserNameAndUserPassword(String userName, String userPassword);

	boolean updateById(SoftUser user);
}