package org.ssh2.core.service;

import java.util.List;

import org.ssh2.core.util.Page;



public interface IBaseService<T> {
	
	void save(T t);  

	void delete(int id);  

	void update(T t); 

	void saveOrUpdate(T t);

	T load(int id);  

	T getUniqueResult(String hql, Object... args);

	List<T> list(String hql, Object...args);

	List<T> getAllFromTable();	// 获取单张表的所有数据

	boolean findForPage(Page<T> page);	// 分页查询

	public boolean findPageByCondition(Page<T> page, String condition);

	void batchDelete(final List<Integer> ids); // 批量删除
}
