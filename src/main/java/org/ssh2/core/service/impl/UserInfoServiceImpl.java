package org.ssh2.core.service.impl;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import org.ssh2.core.dao.IUserInfoDao;
import org.ssh2.core.entity.SoftUser;
import org.ssh2.core.service.IUserInfoService;

/**
 * 用户信息实现类
 * @林景锋
 * 2017年11月23日
 * lv1.0
 *
 */
@Service("userInfoService")
public class UserInfoServiceImpl implements IUserInfoService {


	@Resource(name = "userInfoDao")
	private IUserInfoDao userInfoDao;
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		System.out.println("service login...");
		System.out.println("serddd");
		
		System.out.println("loadUserByUsername="+username);
		if (StringUtils.isNotEmpty(username)) {
			SoftUser user = userInfoDao.getUserByName(username.trim());
			System.out.println("UUU=="+user);
			System.out.println("运行到这里1");
			if (user != null) {
				System.out.println("用户名:"+user.getUsername()+" "+"密码："+user.getPassword());
				System.out.println("运行到这里2");
				return user;
				
			}
		}
		throw new UsernameNotFoundException(
				"Can't not find user while username is '" + username.trim()+ "'");

	}

}
