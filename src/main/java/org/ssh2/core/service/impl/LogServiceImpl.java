package org.ssh2.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.ssh2.core.dao.impl.BaseDaoImpl;
import org.ssh2.core.dao.impl.LogDaoImpl;
import org.ssh2.core.entity.SoftLog;
import org.ssh2.core.service.ILogService;



@Service("logService")
public class LogServiceImpl extends BaseServiceImpl<SoftLog> implements ILogService{

	@SuppressWarnings("unused")
	private LogDaoImpl logDao;
	
	@Autowired(required = true)
	@Qualifier("logDao")
	public void setBaseDao(BaseDaoImpl<SoftLog> baseDao){
		this.baseDao = baseDao;
		this.logDao = (LogDaoImpl) baseDao;
	}
}
