package org.ssh2.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.ssh2.core.dao.impl.BaseDaoImpl;
import org.ssh2.core.dao.impl.ClassroomDaoImpl;
import org.ssh2.core.entity.SoftClassroom;
import org.ssh2.core.service.IClassroomService;

@Service("classroomService")
public class ClassroomServiceImpl extends BaseServiceImpl<SoftClassroom> implements IClassroomService {

	@SuppressWarnings("unused")
	private ClassroomDaoImpl classroomDao;
	@Override
	@Autowired(required = true)
	@Qualifier("classroomDao")
	public void setBaseDao(BaseDaoImpl<SoftClassroom> baseDao) {
		// TODO Auto-generated method stub
		this.baseDao = baseDao;
		this.classroomDao = (ClassroomDaoImpl) baseDao;
	}

}
