package org.ssh2.core.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.ssh2.core.dao.impl.BaseDaoImpl;
import org.ssh2.core.dao.impl.TaskDaoImpl;
import org.ssh2.core.entity.SoftTask;
import org.ssh2.core.service.ITaskService;

@Service("taskService")
public class TaskServiceImpl extends BaseServiceImpl<SoftTask> implements ITaskService {

	private TaskDaoImpl taskDao;

	@Override
	@Autowired(required = true)
	@Qualifier("taskDao")
	public void setBaseDao(BaseDaoImpl<SoftTask> baseDao) {
		// TODO Auto-generated method stub
		this.baseDao = baseDao;
		this.taskDao = (TaskDaoImpl)baseDao;
	}

	@Override
	public List<SoftTask> findTaskByProjectId(int id) {
		System.out.println("int id="+id);
		System.out.println("运行到findTaskByProjectId");
		List<SoftTask> tasks = taskDao.listbyId(id);
		System.out.println(tasks);
		return tasks;
	}

	
	
}
