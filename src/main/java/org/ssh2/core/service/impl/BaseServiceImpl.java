package org.ssh2.core.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.ssh2.core.dao.impl.BaseDaoImpl;
import org.ssh2.core.service.IBaseService;
import org.ssh2.core.util.Page;

@Service("baseService")
@Transactional
public abstract class BaseServiceImpl<T> implements IBaseService<T> {

	protected BaseDaoImpl<T> baseDao;
	public abstract void setBaseDao(BaseDaoImpl<T> baseDao);
	@Override
	public void save(T t) {
		// TODO Auto-generated method stub
		baseDao.save(t);
	}

	@Override
	public void delete(int id) {
		// TODO Auto-generated method stub
		baseDao.delete(id);
	}

	@Override
	public void update(T t) {
		// TODO Auto-generated method stub
		baseDao.update(t);
	}

	@Override
	public void saveOrUpdate(T t) {
		// TODO Auto-generated method stub
		baseDao.save(t);
	}

	@Override
	public T load(int id) {
		// TODO Auto-generated method stub
		return baseDao.load(id);
	}

	@Override
	public T getUniqueResult(String hql, Object... args) {
		// TODO Auto-generated method stub
		return baseDao.getUniqueResult(hql, args);
	}

	@Override
	public List<T> list(String hql, Object... args) {
		// TODO Auto-generated method stub
		return baseDao.list(hql, args);
	}

	@Override
	public List<T> getAllFromTable() {
		// TODO Auto-generated method stub
		return baseDao.getAllFromTable();
	}

	@Override
	public boolean findForPage(Page<T> page) {
		// TODO Auto-generated method stub
		return baseDao.findForPage(page);
	}

	@Override
	public boolean findPageByCondition(Page<T> page, String condition) {
		// TODO Auto-generated method stub
		return baseDao.findPageByCondition(page, condition);
	}

	@Override
	public void batchDelete(List<Integer> ids) {
		// TODO Auto-generated method stub
		baseDao.batchDelete(ids);
	}

}
