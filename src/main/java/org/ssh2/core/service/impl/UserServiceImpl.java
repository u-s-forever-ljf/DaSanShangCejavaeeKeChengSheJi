package org.ssh2.core.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.ssh2.core.dao.impl.BaseDaoImpl;
import org.ssh2.core.dao.impl.UserDaoImpl;
import org.ssh2.core.entity.SoftUser;
import org.ssh2.core.service.IUserService;


/**
 * 用户业务逻辑实现
 * 可以查用户名
 * 林景锋
 * 2017年12月4日
 * lv1.0
 */

@Service("userService")
public class UserServiceImpl extends BaseServiceImpl<SoftUser> implements IUserService {

	private UserDaoImpl userDao;
	@Override
	public SoftUser findByUserName(String userName) {
		List<SoftUser> list = userDao.list("from SoftUser where user_name=?",userName);
		if(list.size()>0){
			return list.get(0);
		}else{
		return null;
		}
	}

	@Override
	public boolean findByUserNameAndUserPassword(String userName, String userPassword) {
		
		List<SoftUser> list = userDao.list("from SoftUser where user_name=? and user_password=?",userName,userPassword);
		if(list.size()>0){
			return  true;
		}else{
			return  false;
		}
		
	}

	
	@Override
	public boolean updateById(SoftUser user) {
		if(user==null){
			return false;
		}else{

			System.out.println("updateById被执行了");
		SoftUser us = userDao.load(user.getId());
		us.setUserTelephone(user.getUserTelephone());
		us.setUserName(user.getUserName());
		us.setUserPassword(user.getUserPassword());
		us.setUser_role(user.getUser_role());
		us.setUserAddress(user.getUserAddress());
		us.setUserEmail(us.getUserEmail());
		us.setUserRealName(us.getUserRealName());
		userDao.update(us);
		return true;
		}
	}

	@Override
    @Autowired(required = true)
    @Qualifier("userDao")
	public void setBaseDao(BaseDaoImpl<SoftUser> baseDao) {
        this.baseDao = baseDao;
        this.userDao = (UserDaoImpl) baseDao;
	}

}
