package org.ssh2.core.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.ssh2.core.dao.impl.BaseDaoImpl;
import org.ssh2.core.dao.impl.ProjectDaoImpl;
import org.ssh2.core.entity.SoftProject;
import org.ssh2.core.entity.SoftUser;
import org.ssh2.core.service.IProjectService;


@Service("projectService")
public class ProjectServiceImpl extends BaseServiceImpl<SoftProject> implements IProjectService{

	@SuppressWarnings("unused")
	private ProjectDaoImpl projectDao;
	
	@Override
	public List<SoftProject> FindByProjectName(String projectName, String value) {
		List<SoftProject> sp=null;
		try{
			 
		sp=projectDao.FindByProjectName(projectName, value);
		}catch(Exception  e){
			System.out.println(e);
		}
		return sp;
		}
	
	@Override
	public List<SoftProject> getAllFromSoftProject() {

		return projectDao.getAllFromSoftProject();
	}

	
	
	@Override
	@Autowired(required = true)
	@Qualifier("projectDao")
	public void setBaseDao(BaseDaoImpl<SoftProject> baseDao) {
		// TODO Auto-generated method stub

		this.baseDao = baseDao;
		this.projectDao = (ProjectDaoImpl) baseDao;
	}


}