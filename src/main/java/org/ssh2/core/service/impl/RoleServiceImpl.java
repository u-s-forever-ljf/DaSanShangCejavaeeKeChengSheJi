package org.ssh2.core.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.ssh2.core.dao.impl.BaseDaoImpl;
import org.ssh2.core.dao.impl.RoleDaoImpl;
import org.ssh2.core.entity.SoftRole;
import org.ssh2.core.service.IRoleService;

@Service("roleService")
public class RoleServiceImpl extends BaseServiceImpl<SoftRole> implements IRoleService {

	@SuppressWarnings("unused")
	private RoleDaoImpl roleDao;
	
	@Override
	@Autowired(required = true)
	@Qualifier("roleDao")
	public void setBaseDao(BaseDaoImpl<SoftRole> baseDao) {
		// TODO Auto-generated method stub
		
		this.baseDao = baseDao;
		this.roleDao = (RoleDaoImpl) baseDao;
	}

}
