package org.ssh2.core.service.impl;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.ssh2.core.dao.impl.BaseDaoImpl;
import org.ssh2.core.dao.impl.SelectCourseDaoImpl;
import org.ssh2.core.entity.SoftSelectCourse;

import org.ssh2.core.service.ISelectCourseService;


@Service("selectCourseService")
public class SelectCourseServiceImpl extends BaseServiceImpl<SoftSelectCourse> implements ISelectCourseService {

	private SelectCourseDaoImpl selectCourseDao;

	@Override
	@Autowired(required = true)
	@Qualifier("selectCourseDao")
	public void setBaseDao(BaseDaoImpl<SoftSelectCourse> baseDao) {
		// TODO Auto-generated method stub

		this.baseDao = baseDao;
		this.selectCourseDao = (SelectCourseDaoImpl) baseDao;
	}

	@Override
	public SoftSelectCourse loadById(int ssid) {
		SoftSelectCourse se=selectCourseDao.loadById(ssid);
      return se;
	}

	@Override
	public List<SoftSelectCourse> find(String name) {
		
		return selectCourseDao.find(name);
	}

	@Override
	public List<SoftSelectCourse> liststatus(String status) {
		System.out.println("statusstatusstatus="+status);
		List<SoftSelectCourse> applyList=selectCourseDao.liststatus(status);
		System.out.println(applyList);
		return applyList;
	}

	//	@Override

	//	public List<SoftSelectCourse> UpdateById(int ssid) {

	//		

	//			

	//			List<SoftSelectCourse> se =  selectCourseDao.list("from SoftSelectCourse WHERE id = ?", ssid);

	//			return se;

	//			

	//		}

}

//	@Override

//	public SoftSelectCourse UpdateById(int id) {

//		// TODO Auto-generated method stub

//		return null;

//	}


