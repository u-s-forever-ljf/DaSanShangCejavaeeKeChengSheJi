package org.ssh2.core.util;

import java.util.List;

//分页工具类

public class Page<T> {
	/*当前页数*/
	private Integer pageNo;
	/*总页数*/
	private Integer totalPage;
	/*单页数据数*/
	private Integer onePageNum;
	/*总记录数*/
	private Long total;
	/*数据列表*/
	private List<T> datas;

	private String clazz;	// 获取跳转页面的分类泛型

	public Page(){
		this.pageNo = 1;
		this.onePageNum = 10;	// 一页显示10条记录
	}

	public Page(Integer onePageNum) {
		this.onePageNum = onePageNum;
	}

	public Integer getPageNo() {
		return pageNo;
	}
	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}



	public Integer getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(Integer totalPage) {
		this.totalPage = totalPage;
	}

	public Integer getOnePageNum() {
		return onePageNum;
	}
	public void setOnePageNum(Integer onePageNum) {
		this.onePageNum = onePageNum;
	}


	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
		//math.ceil(x)返回大于参数x的最小整数,即对浮点数向上取整
		this.totalPage = (int) Math.ceil((double)total/this.onePageNum);
	}

	public List<T> getDatas() {
		return datas;
	}

	public void setDatas(List<T> datas) {
		this.datas = datas;
	}

	public String getClazz() {
		return clazz;
	}

	public void setClazz(String clazz) {
		this.clazz = clazz;
	}

	@Override
	public String toString() {
		return "Page [pageNo=" + pageNo + ", totalPage=" + totalPage
				+ ", onePageNum=" + onePageNum + ", total=" + total
				+ ", datas.size=" + datas.size() + "]";
	}

}
