package org.ssh2.core.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;



/**
 * 班级实体
 */

@Entity
@Table(name = "soft_classroom")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SoftClassroom extends BaseEntity{

	private static final long serialVersionUID = -6114325111022515006L;


	
	@Column(name = "classroom_name")
	private String classroomName;//班级名字

	@Column(name = "classroom_create_user_name")
	private String createUserName; // 创建该角色的用户ID
	
	@Column(name = "classroom_create_date")
	private String createDate;// 加入班级日期

	@ManyToMany(mappedBy = "user_classroom", fetch = FetchType.EAGER)
	private Set<SoftUser> classroom_user; 
	
	
	public String getCreateUserName() {
		return createUserName;
	}


	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}


	public String getCreateDate() {
		return createDate;
	}


	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}


	public Set<SoftUser> getClassroom_user() {
		return classroom_user;
	}


	public void setClassroom_user(Set<SoftUser> classroom_user) {
		this.classroom_user = classroom_user;
	}


	public SoftClassroom(){
		
	}
	

	public String getClassroomName() {
		return classroomName;
	}

	public void setClassroomName(String classroomName) {
		this.classroomName = classroomName;
	}


	
	
}
