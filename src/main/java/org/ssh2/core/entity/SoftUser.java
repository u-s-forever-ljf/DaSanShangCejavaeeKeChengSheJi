package org.ssh2.core.entity;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;




/**
 * 用户实体
 * 林景锋
 * 2017年11月23日
 * lv1.0
 */
@Entity
@Table(name = "soft_user")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SoftUser extends BaseEntity implements UserDetails {

	/**
	 * unique=true是指这个字段的值在这张表里不能重复，所有记录值都要唯一，就像主键那样
	 * nullable=false是这个字段在保存时必需有值，不能还是null值就调用save去保存入库
	 *
	 *private Integer id;
	private String userId;
	private String userName;
	private String userRealName;
	private Integer userSex;
	private Integer userAge;
	private String userPassword;
	private Integer userType;
	private String userAddress;
	private String userTelephone;
	private String userEmail;
	private String userPerId;
	private Integer userPerType;
	private String labId;
	private String sign;
	 */
	private static final long serialVersionUID = -7996992202295686955L;


	@Column(name = "user_create_date")
	private String createDate;// 创建日期

	@Column(name = "user_name", unique = true, nullable = false)
	private String userName;// 用户名

	@Column(name = "user_password")
	private String userPassword;//密码

	@Column(name = "user_telephone")
	private String userTelephone; // 电话

	@Column(name = "user_address")
	private String userAddress;//地址

	@Column(name = "user_email")
	private String userEmail;//邮箱

	@Column(name = "user_realname")
	private String userRealName;//真实姓名

	@Column(name = "user_sex")
	private String userSex;//性别:1-男；2-女





	//inverse表示控制反转，inner join什么的，都是hibernate映射java实体对象类的一些映射约束了什么的
	@ManyToMany(fetch = FetchType.EAGER)//多对多
	@JoinTable(name = "soft_user_role", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id") )
	private Set<SoftRole> user_role;// 所属角色


	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "soft_user_project", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "project_id"))
	private Set<SoftProject> user_project;//所属课程设计

	//	

	@ManyToMany(fetch = FetchType.EAGER)//多对多
	@JoinTable(name = "soft_user_classroom", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "classroom_id") )
	private Set<SoftClassroom> user_classroom;// 所属班级


	public Set<SoftProject> getUser_project() {
		return user_project;
	}

	public void setUser_project(Set<SoftProject> user_project) {
		this.user_project = user_project;
	}


	public Set<SoftClassroom> getUser_classroom() {
		return user_classroom;
	}

	public void setUser_classroom(Set<SoftClassroom> user_classroom) {
		this.user_classroom = user_classroom;
	}

	public SoftUser() {

	}

	public String getCreateDate() {
		return createDate;
	}





	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}





	public String getUserName() {
		return userName;
	}





	public void setUserName(String userName) {
		this.userName = userName;
	}





	public String getUserPassword() {
		return userPassword;
	}





	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}





	public Set<SoftRole> getUser_role() {
		return user_role;
	}





	public void setUser_role(Set<SoftRole> user_role) {
		this.user_role = user_role;
	}





	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Set<GrantedAuthority> auths = new HashSet<>();
		Set<SoftRole> roles = this.getUser_role();
		// 默认所有的用户有"USER"的权利
		auths.add(new SimpleGrantedAuthority("ROLE_USER"));
		System.out.println("auths="+auths);
		for (SoftRole role : roles) {

			auths.add(new SimpleGrantedAuthority(role.getRoleName()));
			System.out.println("roles==="+auths);
		}
		System.out.println("auths==="+auths);
		return auths;
	}

	public String getUserTelephone() {
		return userTelephone;
	}

	public void setUserTelephone(String userTelephone) {
		this.userTelephone = userTelephone;
	}

	public String getUserAddress() {
		return userAddress;
	}

	public void setUserAddress(String userAddress) {
		this.userAddress = userAddress;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserRealName() {
		return userRealName;
	}

	public void setUserRealName(String userRealName) {
		this.userRealName = userRealName;
	}



	public String getUserSex() {
		return userSex;
	}

	public void setUserSex(String userSex) {
		this.userSex = userSex;
	}




	@Override
	public String getPassword() {
		return this.userPassword;
	}

	@Override
	public String getUsername() {
		return this.userName;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}


	//	@OneToMany(mappedBy="document",cascade = {CascadeType.MERGE, CascadeType.PERSIST,
	//			CascadeType.REFRESH})
	//	private Set<SoftDocument> documents;
	//	@OneToMany(mappedBy="document",cascade = {CascadeType.MERGE, CascadeType.PERSIST,
	//			CascadeType.REFRESH})
	//	private Set<SoftDocument> doucumnet = new HashSet<SoftDocument>();

	//	//inverse表示控制反转，inner join什么的，都是hibernate映射java实体对象类的一些映射约束了什么的
	//	@ManyToMany(fetch = FetchType.EAGER)//多对多
	//	@JoinTable(name = "soft_user_document", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "document_id") )
	//	private Set<SoftDocument> user_document;// 所属文档
}
