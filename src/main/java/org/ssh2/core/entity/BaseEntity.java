package org.ssh2.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
/**
 * 公共实体
 * @林景锋  
 * @2017年11月19日
 */
//@MappedSuperclass 
//用在父类上面。
//当这个类肯定是父类时，加此标注。
//如果改成@Entity，则继承后，多个类继承，只会生成一个表，
//而不是多个继承，生成多个表
@MappedSuperclass
public class BaseEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 442368739115632327L;

	@Id
	@Column(name ="id")
	@GeneratedValue(strategy = GenerationType.AUTO)
	protected Integer id;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
}
