package org.ssh2.core.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;


import javax.persistence.ManyToMany;

import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

@Entity
@Table(name = "soft_project")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SoftProject extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1803483756176997723L;

	@Column(name = "project_name", unique = true, nullable = false)
	private String projectName;//课程设计名

	@Column(name = "project_byteacher")
	private String projectByteacher;//所属教师

	@Column(name = "project_create_date")
	private String createDate;// 创建日期

	@Column(name = "project_end_date")
	private String endDate;//结束时间

	@Column(name = "project_desc")
	private String projectDesc; //描述





	@ManyToMany(mappedBy = "user_project")
	private Set<SoftUser> project_user;

	//	@ManyToOne(fetch = FetchType.LAZY)
	//	@JoinColumn(name = "task_id")
	//	private SoftTask task;

	//	@OneToMany(mappedBy="task",cascade = {CascadeType.MERGE, CascadeType.PERSIST,
	//			CascadeType.REFRESH})
	//	private Set<SoftTask> tasks = new HashSet<SoftTask>();
	//	
	//	@OneToMany(mappedBy="document",cascade = {CascadeType.MERGE, CascadeType.PERSIST,
	//			CascadeType.REFRESH})
	//	private Set<SoftDocument> documents = new HashSet<SoftDocument>();
	//	

	@OneToMany(mappedBy="project",cascade = {CascadeType.MERGE, CascadeType.PERSIST,
			CascadeType.REFRESH})
	private Set<SoftTask> tasks;



	public Set<SoftTask> getTasks() {
		return tasks;
	}

	public void setTasks(Set<SoftTask> tasks) {
		this.tasks = tasks;
	}



	public SoftProject(){

	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getProjectByteacher() {
		return projectByteacher;
	}

	public void setProjectByteacher(String projectByteacher) {
		this.projectByteacher = projectByteacher;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	public String getProjectDesc() {
		return projectDesc;
	}

	public void setProjectDesc(String projectDesc) {
		this.projectDesc = projectDesc;
	}



	public Set<SoftUser> getProject_user() {
		return project_user;
	}

	public void setProject_user(Set<SoftUser> project_user) {
		this.project_user = project_user;
	}






}
