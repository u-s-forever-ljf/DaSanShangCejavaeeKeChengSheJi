package org.ssh2.core.entity;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * 角色实体
 * 林景锋
 * 2017年11月23日
 * lv1.0
 */
@Entity
@Table(name = "soft_role")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SoftRole extends BaseEntity {



	/**
	 * 
	 */
	private static final long serialVersionUID = 4417039120169099261L;

	@Column(name = "role_create_date")
	private String createDate; // 创建日期
	
	
	@Column(name = "role_create_user_name")
	private String createUserName; // 创建该角色的用户ID
	
	@Column(name = "role_desc")
	private String roleDesc; // 描述
	
	@Column(name = "role_name")
	private String roleName; // 名称
	
	@ManyToMany(mappedBy = "user_role", fetch = FetchType.EAGER)
	private Set<SoftUser> role_user; 
	
	public SoftRole(){
		
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getCreateUserName() {
		return createUserName;
	}

	public void setCreateUserName(String createUserName) {
		this.createUserName = createUserName;
	}

	public String getRoleDesc() {
		return roleDesc;
	}

	public void setRoleDesc(String roleDesc) {
		this.roleDesc = roleDesc;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public Set<SoftUser> getRole_user() {
		return role_user;
	}

	public void setRole_user(Set<SoftUser> role_user) {
		this.role_user = role_user;
	}
	
	
}
