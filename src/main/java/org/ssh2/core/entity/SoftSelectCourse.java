package org.ssh2.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * 学生用来选课实体
 * 林景锋
 * 2017年12月10日
 * lv1.0
 */

@Entity
@Table(name = "soft_selectCourse")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SoftSelectCourse extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5789306207284837867L;

	@Column(name = "app_create_date")
	private String createDate; // 申请日期
	@Column(name = "app_deal_date")
	private String dealDate; // 处理日期
	
	@Column(name = "project_name")
	private String projectName;//所选的课程设计名
	
	@Column(name = "task_name")
	private String taskName;//所选的课程设计名所包含的任务名

	
	@Column(name = "select_name")
	private String selectName;//选课人
	
	@Column(name = "app_desc")
	private String desc;//学生申请说明
	
	@Column(name ="app_approver")
	private String approver;//审批人
	
	
	@Column(name = "app_status")
	private String status; // 状态(未审批、已审批)
	
	@Column(name = "score")
	private String score;//分数

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getDealDate() {
		return dealDate;
	}

	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}

	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}



	public String getSelectName() {
		return selectName;
	}

	public void setSelectName(String selectName) {
		this.selectName = selectName;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getApprover() {
		return approver;
	}

	public void setApprover(String approver) {
		this.approver = approver;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}
	
}
