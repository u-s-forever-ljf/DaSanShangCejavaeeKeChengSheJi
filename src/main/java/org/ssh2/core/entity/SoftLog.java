package org.ssh2.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;



/**
 * 日志信息实体
 * 林景锋
 * 2017年11月26日
 * 
 */

@Entity
@Table(name = "soft_log")
public class SoftLog extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4503008436807851812L;

	
	@Column(name = "log_clazz")
	private String clazz; 		// 操作类
	
	@Column(name = "log_ip_add")
	private String ipAdd;		// 用户IP地址
	
	@Column(name = "log_method")
	private String method;		// 操作方法

	
	@Column(name = "log_msg")
	private String msg;			// 操作信息
	

	@Column(name = "log_username")
	private String username;	// 操作用户
	
	@Column(name = "log_create_date")
	private String createDate;	//创建时间

	public SoftLog(){
		
	}
	public String getClazz() {
		return clazz;
	}

	public void setClazz(String clazz) {
		this.clazz = clazz;
	}

	public String getIpAdd() {
		return ipAdd;
	}

	public void setIpAdd(String ipAdd) {
		this.ipAdd = ipAdd;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}


	
}
