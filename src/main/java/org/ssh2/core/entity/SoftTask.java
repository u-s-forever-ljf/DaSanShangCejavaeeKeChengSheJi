package org.ssh2.core.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
/**
 * 
 * 3.软件工程课程设计信息管理系统
目的和背景：软件工程课程设计主要训练学生采用工程的概念、
原理、技术和方法开发和维护软件的能力。
通过独立设计和实现一个小型信息管理系统，使学生深入理解软件生存周期内若干阶段的主要任务
、以及所用的方法和步骤，掌握软件生产的重要步骤、工程化的原理、方法和相关技术，
提高将工程应用软件计划、开发和维护的能力。
主要内容：
1.用户登录信息管理
2.学生信息管理
3.教师信息管理
4.题目信息管理
5.信息的组合查询、模糊查询
6.题目的选取与审批
 * @author 林景锋
 *
 */


@Entity
@Table(name = "soft_task")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class SoftTask extends BaseEntity {

	/**
编号	Id
任务名	task_name
所属课程设计	project_name
开始时间	task_create_time
结束时间	task_end_time
状态	task_status
分数	task_store
任务文档	task_document
	 */
	private static final long serialVersionUID = -7740274652896112839L;

	@Column(name = "task_name", unique = true, nullable = false)
	private String taskName;//任务名


	@Column(name = "task_byteacher")
	private String taskByteacher;//所属教师

	@Column(name = "task_create_date")
	private String createDate;//开始时间

	@Column(name = "task_end_date")
	private String endDate;//结束时间
	@Column(name = "task_desc")
	private String taskDesc;//描述
	


	

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "project_id")
	private SoftProject project;
	
	
	public SoftTask(){
		
	}
	
	
	public String getTaskDesc() {
		return taskDesc;
	}


	public void setTaskDesc(String taskDesc) {
		this.taskDesc = taskDesc;
	}


	public String getTaskByteacher() {
		return taskByteacher;
	}


	public void setTaskByteacher(String taskByteacher) {
		this.taskByteacher = taskByteacher;
	}


	public SoftProject getProject() {
		return project;
	}


	public void setProject(SoftProject project) {
		this.project = project;
	}


	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}



//	
//	@OneToMany(mappedBy="task",cascade = {CascadeType.MERGE, CascadeType.PERSIST,
//			CascadeType.REFRESH})
//	private Set<SoftProject> projects;

	
//    @OneToMany(mappedBy = "model", 
//            fetch = FetchType.LAZY)
//    private List<CmsModelField> fieldList;

//	@ManyToOne(fetch = FetchType.LAZY)
//	@JoinColumn(name = "project_id")
//	private SoftProject project;
	
}
